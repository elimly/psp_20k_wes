#!/bin/bash


echo "Started: run_extract_pVCF.psp_adsp_ctrl.20kWES.20504_2020_0626.filter1.sh"

## ADSP 20kWES 20504.2020.0626 pVCF file info 
DIR_VCF="/mnt/adsp/ftpsite/ftp_UTH_TMC/gcad.r2.wes.qc.pvcf.20504.2020.0626/pVCF"
VCF_PREFEX="gcad.qc.r2.wes."
VCF_SUFFIX=".20504.GATK.2020.06.26.biallelic.genotypes.ALL.vcf.gz"


## Samples & variants of interest
DIR_TARGET="/mnt/adsp/users/psp_hg38_wes/data/20kWES_02_qc_flag_filtered/snv/filter1_bcftools_targets_PASS-monomorphic_VFLAGS-0-3"

FILE_SAMPLE="/mnt/adsp/users/psp_hg38_wes/data/20kWES_02_qc_flag_filtered/samples/bcftools_samples_files/psp_adsp.ADSP_ctrl_samples-ALL.20504.2020.0626.tsv"

## output settings
DIR_OUT="/mnt/adsp/users/psp_hg38_wes/data/pVCF/psp_adsp.filter1_extract_from_20kWES"

OUT_TYPE="b"
OUT_NAME="psp_adsp.20504.filter1.VFLAGS-0-3"

## command variables
PVCF_HELPER_SCRIPT="/mnt/adsp/users/psp_hg38_wes/src/post_QC_processing/pVCF_processing/bcftools_extract_pVCF_variants_samples.sh"

BCFTOOLSCMD="/usr/local/bin/bcftools"



mkdir -p $DIR_OUT


## pVCF format
# "gcad.qc.r2.wes.chr1.20504.GATK.2020.06.26.biallelic.genotypes.ALL.vcf.gz"

## target format
# "psp_adsp.20504.filter1.targets_pass_qc.chr1.tsv"


echo "call helper script: ${PVCF_HELPER_SCRIPT}"

## run extract pVCF helper script on each chromosome pVCF file
for CURR_TARGET in $DIR_TARGET/* ; do
	FILE=$(basename "$CURR_TARGET")
	CURR_CHR=$(echo $CURR_TARGET | grep -o 'chr[0-9]\{1,2\}')

	echo " "
	echo $CURR_CHR
	echo $FILE

	CURR_VCF="${DIR_VCF}/${VCF_PREFEX}${CURR_CHR}${VCF_SUFFIX}"
	CURR_OUT="${DIR_OUT}/${OUT_NAME}.${CURR_CHR}.vcf.gz"	

	echo $CURR_VCF
	echo $CURR_OUT

	/bin/bash "$PVCF_HELPER_SCRIPT" "${CURR_VCF}" "${CURR_TARGET}" "${FILE_SAMPLE}" "${CURR_OUT}" "${OUT_TYPE}" "${BCFTOOLSCMD}"


done


echo " "
echo " "
echo "Complete. Goodbye."

