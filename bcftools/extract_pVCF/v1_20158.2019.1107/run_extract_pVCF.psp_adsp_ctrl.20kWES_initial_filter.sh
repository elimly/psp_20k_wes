#!/bin/bash
echo "Started: run_extract_pVCF.psp_adsp_ctrl.20kWES_initial_filter.sh"

DIR_VCF="/mnt/adsp/ftpsite/ftp_UTH_TMC/gcad.r2.wes.qc.pvcf.20158.2019.1107/pVCF"
DIR_TARGET="/mnt/adsp/users/psp_hg38_wes/data/02_post_qc/snv/filter1_bcftools_targets_PASS-monomorphic_VFLAGS-0-3"
DIR_OUT="/mnt/adsp/users/psp_hg38_wes/data/03_filtered_pVCF/psp_adsp_ctrl.pVCF.filter1_extract_from_20kWES"
FILE_SAMPLE="/mnt/adsp/users/psp_hg38_wes/data/02_post_qc/samples/bcftools_samples_files/psp_adsp.samples-ALL.2020-02-12.tsv"
# FILE_SAMPLE="/mnt/adsp/users/psp_hg38_wes/data/02_post_qc/samples/bcftools_samples_files/psp_adsp.samples_PASS_QC.tsv"


## get script absolute path
DIR_SCRIPT=$(dirname "$(readlink -f "$0")")
PVCF_HELPER_SCRIPT="${DIR_SCRIPT}"/"extract_pVCF_variants_samples.sh"


## run extract pVCF helper script on each chromosome pVCF file
for CURR_TARGET in $DIR_TARGET/* ; do
	FILE=$(basename "$CURR_TARGET")
	CURR_CHR="chr"${FILE//[^0-9]/}
	echo " "
	echo $CURR_CHR
	echo $FILE

	## VCF file name format
	##"gcad.qc.r2.wes.chr.20158.GATK.2019.11.07.biallelic.genotypes.ALL.vcf.gz"
	CURR_VCF=${DIR_VCF}/"gcad.qc.r2.wes."${CURR_CHR}".20158.GATK.2019.11.07.biallelic.genotypes.ALL.vcf.gz"

	CURR_OUT=${DIR_OUT}/"psp_adsp.PASS-monomorphic_VFLAGS-0-3."${CURR_CHR}".vcf.gz"
	# CURR_OUT=${DIR_OUT}/"psp_adsp.intersect_pass_qc."${CURR_CHR}".vcf.gz"

	echo $CURR_VCF
	echo $CURR_OUT

	sh $PVCF_HELPER_SCRIPT $CURR_VCF $CURR_TARGET $FILE_SAMPLE $CURR_OUT

done


echo " "
echo " "
echo "Complete. Goodbye."

