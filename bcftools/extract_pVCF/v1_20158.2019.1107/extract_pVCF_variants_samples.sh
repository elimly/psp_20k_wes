#!/bin/bash
VCF_FILE=$1
TARGET_FILE=$2
SAMPLE_FILE=$3
OUT_FILE=$4

## Note: helper script does NOT require pVCF file to be indexed but is slower as a result

#### single chromosome
echo "vcf:  "$VCF_FILE
echo "target:  "$TARGET_FILE
echo "sample:  "$SAMPLE_FILE
echo "out:  "$OUT_FILE
echo " "


echo "before bcftools call"
bcftools view --no-version \
	--targets-file $TARGET_FILE \
	--samples-file $SAMPLE_FILE \
	--output-type b \
	--output-file $OUT_FILE \
	$VCF_FILE

echo "after bcftools call"
