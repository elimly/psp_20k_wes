#!/bin/bash
VCF_FILE=$1
TARGET_FILE=$2
SAMPLE_FILE=$3
OUT_FILE=$4
OUT_TYPE=$5
BCFTOOLSCMD=$6

#### single chromosome
echo "vcf:  "$VCF_FILE
echo "target:  "$TARGET_FILE
echo "sample:  "$SAMPLE_FILE
echo "out:  "$OUT_FILE
echo "bcftools cmd:  "$BCFTOOLSCMD
echo " "

echo "before bcftools call"
$BCFTOOLSCMD view --no-version \
	-R $TARGET_FILE -T $TARGET_FILE \
	--samples-file $SAMPLE_FILE \
	--output-type $OUT_TYPE \
	--output-file $OUT_FILE \
	$VCF_FILE

echo "after bcftools call"
