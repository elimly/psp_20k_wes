#!/bin/bash

#$ -cwd -V
#$ -S /bin/bash
#$ -o qsubout/extract_20kWES/ 
#$ -e qsubout/extract_20kWES/ 


##### dynamic variables
## ADSP 20kWES 20504.2020.0626 pVCF file  
VCF_PREFIX="/mnt/adsp/ftpsite/ftp_UTH_TMC/gcad.r2.wes.qc.pvcf.20504.2020.0626/pVCF/gcad.qc.r2.wes.chr"
VCF_SUFFIX=".20504.GATK.2020.06.26.biallelic.genotypes.ALL.vcf.gz"

## bcftools target file
TARGET_PREFIX="/mnt/adsp/users/psp_hg38_wes/data/20kWES_02_qc_flag_filtered/snv/filter1_bcftools_targets_PASS-monomorphic_VFLAGS-0-3/psp_adsp.20504.filter1.targets_pass_qc.chr"
TARGET_SUFFIX=".tsv"

## output file
OUT_PREFIX="/mnt/adsp/users/psp_hg38_wes/data/pVCF/psp_adsp.filter1_extract_from_20kWES/psp_adsp.20504.filter1.VFLAGS-0-3.chr"

# OUT_DIR="/mnt/adsp/users/psp_hg38_wes/data/pVCF/psp_adsp.filter1_extract_from_20kWES"
# OUT_PREFIX="${OUT_DIR}/psp_adsp.20504.filter1.VFLAGS-0-3.chr"
OUT_SUFFIX=".vcf.gz"

###########################################################
##### setup variables for job array
TARGET_FILE=${TARGET_PREFIX}${SGE_TASK_ID}${TARGET_SUFFIX}
VCF_FILE=${VCF_PREFIX}${SGE_TASK_ID}${VCF_SUFFIX}
OUT_FILE=${OUT_PREFIX}${SGE_TASK_ID}${OUT_SUFFIX}
###########################################################

##### static variables
OUT_TYPE="b"

## Samples of interest  * does NOT change *
SAMPLE_FILE="/mnt/adsp/users/psp_hg38_wes/data/20kWES_02_qc_flag_filtered/samples/bcftools_samples_files/psp_adsp.samples-ALL.20504.2020.0626.tsv"

## command variables 
PVCF_HELPER_SCRIPT="/mnt/adsp/users/psp_hg38_wes/src/post_QC_processing/pVCF_processing/bcftools_extract_pVCF_variants_samples.sh"

BCFTOOLSCMD="/usr/local/bin/bcftools"

#---------------------------------------------------------#

mkdir -p $OUT_DIR

## run bcftools helper script on current target + pVCF file
sh "$PVCF_HELPER_SCRIPT" "${VCF_FILE}" "${TARGET_FILE}" "${SAMPLE_FILE}" "${OUT_FILE}" "${OUT_TYPE}" "${BCFTOOLSCMD}"


