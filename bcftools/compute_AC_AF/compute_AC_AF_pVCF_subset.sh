#!/bin/bash

###############################################################################
## NOTE: Old version of bcftools +fill-tags plugin installed on OLD-METHOD-C
##			that lacks options -S | -l, and does NOT have MAF INFO tag
##
## >> this script is a hacky way to compute AC/AF on subset of samples 
## 			WITHOUT using the -S option 
###############################################################################

VCF_FILE=$1
SAMP_FILE=$2
OUT_VCF=$3
OUT_MAF=$4
MULTIALLELIC=$5

## current chromosome pVCF 
echo "current vcf:  "$VCF_FILE
echo "output files:"
echo "$OUT_VCF"
echo "$OUT_MAF"
echo " "


#### Run bcftools commands

## extract subset of samples --> new pVCF
bcftools view $VCF_FILE --force-samples -S $SAMP_FILE -Ou | bcftools +fill-tags -Ou > ${OUT_VCF}


## if multiallelic: normalize --> rerun AC/AF bcftools query
if [ "$MULTIALLELIC" == "true" ];
then
  echo -e "\tmulti-allelic vcf --> normalize \n"

  ## normalize multi-allelic variants --> 1 row per ALT allele
  bcftools norm -m-any ${OUT_VCF} -Ov > ${OUT_VCF}".norm.vcf"
  ## compute AC, AN, AF, etc... on sample subset
  bcftools query -f '%CHROM\t%POS\t%ID\t%REF\t%ALT\t%AC\t%AF\t%AN\t%AC_Hom\t%AC_Het\t%AC_Hemi\t%NS\n' ${OUT_VCF}".norm.vcf" | awk -F"\t" -v OFS='\t' 'BEGIN {print "CHROM\tPOS\tID\tREF\tALT\tAC\tAF\tAN\tAC_Hom\tAC_Het\tAC_Hemi\tNS"} !/^#/ {print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12}' > ${OUT_MAF}".norm.tsv"
else
  echo -e "\tnot multi-allelic \n"

  ## compute AC, AN, AF, etc... on sample subset
  bcftools query -f '%CHROM\t%POS\t%ID\t%REF\t%ALT\t%AC\t%AF\t%AN\t%AC_Hom\t%AC_Het\t%AC_Hemi\t%NS\n' ${OUT_VCF} | awk -F"\t" -v OFS='\t' 'BEGIN {print "CHROM\tPOS\tID\tREF\tALT\tAC\tAF\tAN\tAC_Hom\tAC_Het\tAC_Hemi\tNS"} !/^#/ {print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12}' > ${OUT_MAF}
fi  



