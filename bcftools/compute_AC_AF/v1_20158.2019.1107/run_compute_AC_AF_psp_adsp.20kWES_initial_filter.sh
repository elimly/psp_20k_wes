#!/bin/bash

######### Driver script to run compute_AC_AF_psp_adsp.sh

# DIR_VCF="/mnt/adsp/users/psp_hg38_wes/data/03_filtered_pVCF/test_vcf"
# DIR_OUT="/mnt/adsp/users/psp_hg38_wes/data/04_MAC_MAF/test"

## specify file and directory paths
DIR_VCF="/mnt/adsp/users/psp_hg38_wes/data/03_filtered_pVCF/psp_adsp.pVCF.20kWES_initial_filter"
SAMP_FILE_PSP="/mnt/adsp/users/psp_hg38_wes/data/02_post_qc/samples/bcftools_samples_files/psp_adsp.samples_PASS_QC.PSP.tsv"
SAMP_FILE_ADSP="/mnt/adsp/users/psp_hg38_wes/data/02_post_qc/samples/bcftools_samples_files/psp_adsp.samples_PASS_QC.ADSP.tsv"
SAMP_NAME="samples_PASS_QC"
DIR_OUT="/mnt/adsp/users/psp_hg38_wes/data/04_MAC_MAF/psp_adsp.AC_AF.20kWES_initial_filter"

## get script absolute path
DIR_SCRIPT=$(dirname "$(readlink -f "$0")")
AC_AF_SCRIPT="${DIR_SCRIPT}"/"compute_AC_AF_psp_adsp.sh"

## run script with current data set
sh $AC_AF_SCRIPT "$DIR_VCF" "$SAMP_FILE_PSP" "$SAMP_FILE_ADSP" "$SAMP_NAME" "$DIR_OUT"

