#!/bin/bash
DIR_VCF=$1
SAMP_FILE_PSP=$2
SAMP_FILE_ADSP=$3
SAMP_NAME=$4
DIR_OUT=$5

echo "compute_pVCF_AC_AF.sh started"
echo " "
echo "pVCF dir: "$DIR_VCF
echo "sample PSP:  "$SAMP_FILE_PSP
echo "sample ADSP:  "$SAMP_FILE_ADSP
echo "sample file name:  "$SAMP_NAME
echo "output dir:  "$DIR_OUT

## get script absolute path
DIR_SCRIPT=$(dirname "$(readlink -f "$0")")
PVCF_HELPER_SCRIPT="${DIR_SCRIPT}"/"compute_pVCF_AC_AF.psp_adsp.sh"


## run helper script on each chrom pVCF file:
for VCF_FILE in $DIR_VCF/* ; do
	echo " "
	echo $VCF_FILE

	sh $PVCF_HELPER_SCRIPT "$VCF_FILE" "$SAMP_FILE_PSP" "$SAMP_FILE_ADSP" "$SAMP_NAME" "$DIR_OUT"
done


echo "compute_pVCF_AC_AF.sh complete - goodbye."
