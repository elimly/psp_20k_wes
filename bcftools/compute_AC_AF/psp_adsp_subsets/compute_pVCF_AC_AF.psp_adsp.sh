#!/bin/bash

###############################################################################
## NOTE: Old version of bcftools +fill-tags plugin installed on OLD-METHOD-C
##			that lacks options -S | -l, and does NOT have MAF INFO tag
##
## >> this script is a hacky way to compute AC/AF on subset of samples 
## 			WITHOUT using the -S option 
###############################################################################

VCF_FILE=$1
SAMP_FILE_PSP=$2
SAMP_FILE_ADSP=$3
SAMP_NAME=$4
DIR_OUT=$5

## current chromosome pVCF 
echo "current vcf:  "$VCF_FILE

## AC AF output files
MAF_FILE_PSP=${DIR_OUT}/$(basename "$VCF_FILE")${SAMP_NAME}".AC_AF.PSP.tsv"
MAF_FILE_ADSP=${DIR_OUT}/$(basename "$VCF_FILE")${SAMP_NAME}".AC_AF.ADSP_ctrl.tsv"

echo "output files:"
echo "$MAF_FILE_PSP"
echo "$MAF_FILE_ADSP"
echo " "


#### Run bcftools commands

## extract PSP samples & compute AC, AN, AF, etc...
bcftools view $VCF_FILE -S $SAMP_FILE_PSP -Ou | bcftools +fill-tags -Ou | bcftools query -f '%CHROM\t%POS\t%REF\t%ALT\t%AC\t%AF\t%AN\t%AC_Hom\t%AC_Het\t%AC_Hemi\t%NS\n' | awk -F"\t" -v OFS='\t' 'BEGIN {print "CHROM\tPOS\tREF\tALT\tAC\tAF\tAN\tAC_Hom\tAC_Het\tAC_Hemi\tNS"} !/^#/ {print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11}' > $MAF_FILE_PSP


## extract ADSP control samples & compute AC, AN, AF, etc... 
bcftools view $VCF_FILE -S $SAMP_FILE_ADSP -Ou | bcftools +fill-tags -Ou | bcftools query -f '%CHROM\t%POS\t%REF\t%ALT\t%AC\t%AF\t%AN\t%AC_Hom\t%AC_Het\t%AC_Hemi\t%NS\n' | awk -F"\t" -v OFS='\t' 'BEGIN {print "CHROM\tPOS\tREF\tALT\tAC\tAF\tAN\tAC_Hom\tAC_Het\tAC_Hemi\tNS"} !/^#/ {print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11}' > $MAF_FILE_ADSP


