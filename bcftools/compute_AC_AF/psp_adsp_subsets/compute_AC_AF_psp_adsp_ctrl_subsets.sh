#!/bin/bash
DIR_VCF=$1
SAMP_FILE_PSP=$2
SAMP_FILE_ADSP=$3
SAMP_FILE_ADSP_N=$4
SAMP_FILE_ADSP_I=$5
SAMP_NAME=$6
DIR_OUT=$7

echo "compute_pVCF_AC_AF.sh started"
echo " "
echo "pVCF dir: "$DIR_VCF
echo "sample PSP:  "$SAMP_FILE_PSP
echo "sample ADSP:  "$SAMP_FILE_ADSP
echo "sample ADSP Nimbelgen:  "$SAMP_FILE_ADSP_N
echo "sample ADSP Illumina:  "$SAMP_FILE_ADSP_I
echo "sample file name:  "$SAMP_NAME
echo "output dir:  "$DIR_OUT

## get script absolute path
DIR_SCRIPT=$(dirname "$(readlink -f "$0")")
PVCF_HELPER_SCRIPT="${DIR_SCRIPT}"/"compute_AC_AF_on_pVCF_subset.sh"


## run helper script on each chrom pVCF file:
for VCF_FILE in $DIR_VCF/* ; do
	echo " "
	echo $VCF_FILE

	sh $PVCF_HELPER_SCRIPT "$VCF_FILE" "$SAMP_FILE_PSP" "$SAMP_NAME" "PSP" "$DIR_OUT"
	sh $PVCF_HELPER_SCRIPT "$VCF_FILE" "$SAMP_FILE_ADSP" "$SAMP_NAME" "ADSP" "$DIR_OUT"
	sh $PVCF_HELPER_SCRIPT "$VCF_FILE" "$SAMP_FILE_ADSP_N" "$SAMP_NAME" "ADSP_Nimbelgen" "$DIR_OUT"
	sh $PVCF_HELPER_SCRIPT "$VCF_FILE" "$SAMP_FILE_ADSP_I" "$SAMP_NAME" "ADSP_Illumina" "$DIR_OUT"

done


echo "compute_AC_AF_psp_adsp_ctrl_subsets.sh complete - goodbye."
