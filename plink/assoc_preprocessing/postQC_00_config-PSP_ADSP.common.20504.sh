#!/bin/bash

## postQC_00_config-PSP_ADSP.common.20504.sh

###############################################################################
##### User supplied variables 
###############################################################################

## specify Cohort - specific values
# DIR_PLINK1="/mnt/adsp/users/psp_hg38_wes/data/PLINK/eigenstrat_case_ctrl_common/common_outlier_removed"
DIR_PLINK1="/mnt/adsp/users/psp_hg38_wes/data/PLINK/filter1_extract_from_20kWES"
DIR_PLINK2="/mnt/adsp/users/psp_hg38_wes/data/PLINK/filter2_post_common_PCA_QC"

PLINK_NAME1="psp_adsp_filter1_merge"
PLINK_NAME2="psp_adsp.common_PCA.pass_qc"

## specify Pass QC sample cohort filess
FILE_SAMPLE_PASS="/mnt/adsp/users/psp_hg38_wes/data/cohort_files/sample_lists/psp_adsp.common_PCA.pass_qc.samples.keep_indiv.id"
FILE_PASS_FAM="/mnt/adsp/users/psp_hg38_wes/data/cohort_files/fam_pheno_covar/psp_adsp.common_PCA.pass_qc.fam"
FILE_COVAR="/mnt/adsp/users/psp_hg38_wes/data/cohort_files/fam_pheno_covar/psp_adsp.common_PCA.pass_qc.pheno.20504.2020.0626.txt"

## specify software & environment settings
# PLINKCMD="/usr/local/bin/plink-1.90b6.9 "
PLINKCMD="/home/elimly/bin/plink-1.9/plink "

AWK="awk"


###############################################################################
##### Derived variables 
###############################################################################

DIR_PLINK_STAT="${DIR_PLINK2}"/stat
DIR_PLINK_SNPLIST="${DIR_PLINK2}"/snplist
DIR_PLINK_GWAS="${DIR_PLINK2}"/GWAS

PLINK_PREFIX1="${DIR_PLINK1}"/"${PLINK_NAME1}"
PLINK_PREFIX2="${DIR_PLINK2}"/"${PLINK_NAME2}"
PLINK_PREFIX3="${DIR_PLINK_STAT}"/"${PLINK_NAME2}"
PLINK_PREFIX4="${DIR_PLINK_SNPLIST}"/"${PLINK_NAME2}"
PLINK_PREFIX5="${DIR_PLINK_GWAS}"/"${PLINK_NAME2}_assoc"


