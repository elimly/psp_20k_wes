#!/bin/bash

## eigenstrat00_1000G_config-PSP_ADSP.20504.sh

###############################################################################
##### User supplied variables 
###############################################################################

## specify workspace directory
DIR_WS="/mnt/adsp/users/psp_hg38_wes/data"


## specify Cohort - specific values
DIR_PLINK1="/mnt/adsp/users/psp_hg38_wes/data/PLINK/filter1_extract_from_20kWES"
DIR_PLINK2="/mnt/adsp/users/psp_hg38_wes/data/PLINK/filter2_post_QC"
PLINK_NAME1="psp_adsp_filter1_merge"
PLINK_NAME2="psp_adsp.pass_qc"


FILE_SAMPLE_PASS="/mnt/adsp/users/psp_hg38_wes/data/cohort_files/sample_lists/psp_adsp.pass_qc.samples.keep_indiv.id"

FILE_PASS_FAM="/mnt/adsp/users/psp_hg38_wes/data/cohort_files/fam_pheno_covar/psp_adsp.pass_qc.fam"

FILE_COVAR="/mnt/adsp/users/psp_hg38_wes/data/cohort_files/fam_pheno_covar/psp_adsp.pass_qc.pheno.20504.2020.0626.txt"





## specify software & environment settings
PLINKCMD="/usr/local/bin/plink-1.90b6.9 "
AWK="awk"



###############################################################################
##### Derived variables 
###############################################################################

PLINK_PREFIX1="${DIR_PLINK1}"/"${PLINK_NAME1}"
PLINK_PREFIX2="${DIR_PLINK2}"/"${PLINK_NAME2}"
PLINK_PREFIX3="${DIR_PLINK2}"/"${PLINK_NAME2}_assoc"

mkdir -p ${DIR_PLINK2}

# echo " "
# echo "before PLINK filter cmd..."

# $PLINKCMD --bfile $PLINK_PREFIX1 \
# 		--keep $FILE_SAMPLE_PASS \
# 		--recode --make-bed \
# 		--out $PLINK_PREFIX2 

# echo "... after PLINK filter cmd."

# echo "replace .fam"
# cp $FILE_PASS_FAM "${DIR_PLINK2}/${PLINK_NAME2}.fam"


echo " "
echo "start --assoc"
$PLINKCMD --bfile $PLINK_PREFIX2 --assoc --covar $FILE_COVAR --covar-name PC01,PC02,PC03,H1H2_dosage --out $PLINK_PREFIX3 

echo "after assoc "

# plink --assoc --covar /path/covar.txt --covar-name pc1,pc2,pc3,h1h2

# $PLINKCMD --bfile $COHORT_PREFIX1 --extract $COMMON_SNPLIST --recode --make-bed --out $COHORT_PREFIX3 


