#!/bin/bash

## postQC_01_filter_pass_QC-PSP_ADSP.common.20504.sh

source postQC_00_config-PSP_ADSP.common.20504.sh

mkdir -p ${DIR_PLINK2} ${DIR_PLINK_STAT} ${DIR_PLINK_SNPLIST}


###############################################################################
##### Step 1: extract samples that Pass QC
###############################################################################
echo " "
echo "Step 1: extract samples that Pass QC"

$PLINKCMD --bfile $PLINK_PREFIX1 \
		--keep $FILE_SAMPLE_PASS \
		--recode --make-bed \
		--out $PLINK_PREFIX2 


echo "replace .fam"
cp $FILE_PASS_FAM "${DIR_PLINK2}/${PLINK_NAME2}.fam"



###############################################################################
##### Step 2: compute pass QC Allele frequency 
###############################################################################
echo " "
echo "Step 2: compute pass QC Allele frequency "

echo "Cohort compute frequency"
$PLINKCMD --bfile $PLINK_PREFIX2 --freq --out "${PLINK_PREFIX3}"_maf
$PLINKCMD --bfile $PLINK_PREFIX2 --freq case-control --out "${PLINK_PREFIX3}"_maf_case-control
$PLINKCMD --bfile $PLINK_PREFIX2 --freq counts --out "${PLINK_PREFIX3}"_maf
$PLINKCMD --bfile $PLINK_PREFIX2 --freqx --out "${PLINK_PREFIX3}"_maf

###############################################################################
##### Step 3: filter SNP lists MAF >= 0.05, 0.01, 0.005  
###############################################################################
echo " "
echo "Step 3: filter SNP lists MAF >= 0.05, 0.01, 0.005 + MAF < 0.05 0.01 0.005 "

$AWK '$5>=0.05 {print $2}' "${PLINK_PREFIX3}"_maf.frq > "${PLINK_PREFIX4}".maf_ge_0.05.id
echo 'Number of markers with MAF >= 0.05: '`wc -l "${PLINK_PREFIX4}".maf_ge_0.05.id`

$AWK '$5<0.05 {print $2}' "${PLINK_PREFIX3}"_maf.frq > "${PLINK_PREFIX4}".maf_under_0.05.id
echo 'Number of markers with MAF < 0.05: '`wc -l "${PLINK_PREFIX4}".maf_under_0.05.id`

$AWK '$5>=0.01 {print $2}' "${PLINK_PREFIX3}"_maf.frq > "${PLINK_PREFIX4}".maf_ge_0.01.id
echo 'Number of markers with MAF >= 0.01: '`wc -l "${PLINK_PREFIX4}".maf_ge_0.01.id`

$AWK '$5<0.01 {print $2}' "${PLINK_PREFIX3}"_maf.frq > "${PLINK_PREFIX4}".maf_under_0.01.id
echo 'Number of markers with MAF < 0.01: '`wc -l "${PLINK_PREFIX4}".maf_under_0.01.id`

$AWK '$5>=0.005 {print $2}' "${PLINK_PREFIX3}"_maf.frq > "${PLINK_PREFIX4}".maf_ge_0.005.id
echo 'Number of markers with MAF >= 0.005: '`wc -l "${PLINK_PREFIX4}".maf_ge_0.005.id`

$AWK '$5<0.005 {print $2}' "${PLINK_PREFIX3}"_maf.frq > "${PLINK_PREFIX4}".maf_under_0.005.id
echo 'Number of markers with MAF < 0.005: '`wc -l "${PLINK_PREFIX4}".maf_under_0.005.id`



###############################################################################
##### Step 4: compute missingness 
###############################################################################
echo " "
echo "Step 4: compute missingness  "


$PLINKCMD --bfile $PLINK_PREFIX2 --missing --out "${PLINK_PREFIX3}"_missing

$AWK '$2!="SNP" && $5<=0.02 {print $2}' "${PLINK_PREFIX3}"_missing.lmiss > "${PLINK_PREFIX4}".lmiss_le_0.02.id
echo 'Number of markers with lmiss <= 0.02: '`wc -l "${PLINK_PREFIX4}".lmiss_le_0.02.id`

$AWK '$2!="SNP" && $5<=0.05 {print $2}' "${PLINK_PREFIX3}"_missing.lmiss > "${PLINK_PREFIX4}".lmiss_le_0.05.id
echo 'Number of markers with lmiss <= 0.05: '`wc -l "${PLINK_PREFIX4}".lmiss_le_0.05.id`




echo "... postQC_01_filter_pass_QC-PSP_ADSP.common.20504.sh complete. "

