# coding: utf-8


## Filter SNVs for PSP hg38 WES analysis
# 
# 1) intersection of Nimblegen & Illumina Rapid capture regions  
# 2) pass in ALL 3 subsets  
# 

# Preferred System environment:
# - Python >3.6    
# - Pandas >0.24.0


##### Imports & Setup
import os, sys, pathlib
import pandas as pd





#### Define QC pipeline SNV filtering variables
## PSP & ADSP QC companion snv.summary file names
PSP_NIMBLEGEN="summary.snv.PSP-Nimblegen_VCRome_V2.1.tsv"
ADSP_ILLUMINA="summary.snv.ADSP_Discovery-Illumina_Rapid_Capture_Exome_(ICE)_kit.tsv"
ADSP_NIMBLEGEN="summary.snv.ADSP_Discovery-Nimblegen_VCRome_V2.1.tsv"

## SNV Filter variable - Capture kit target region
FILT_TARGET_COL = 'InTargetRegion'
FILT_TARGET_VAL = 1

## SNV Filter variable - Pass QC
FILT_QCPASS_COL = 'FilteredOut'
FILT_QCPASS_VAL = 0

## alternative to FILT_QCPASS_COL / FILT_QCPASS_VAL
FILT_QCFLAG_COL = 'VFLAGS'
FILT_QCFLAG_VAL = '0'

COLS_FILTER = [FILT_TARGET_COL, FILT_QCPASS_COL, FILT_QCFLAG_COL]




### Functions
## TODO: refactor individual file counts
def process_snv_summary_file(file_snv, cohort, platform):
	df_in = pd.read_table(file_snv, sep='\t')
	
	count_variables = ['off_target', 'in_target', 'in_target_pass_qc']
	_df = pd.DataFrame({'cohort':cohort, 'platform':platform, 'snv_subset':count_variables})
	_df.loc[_df['snv_subset']=='off_target', 'count'] = df_in[df_in['InTargetRegion']==0].shape[0]
	_df.loc[_df['snv_subset']=='in_target', 'count'] = df_in[df_in['InTargetRegion']==1].shape[0]
	_df.loc[_df['snv_subset']=='in_target_pass_qc', 'count'] = df_in[(df_in['InTargetRegion']==1) & (df_in['FilteredOut']==0)].shape[0]

	return df_in, _df



def merge_psp_adsp_snv_dataset(curr_dir, file_psp, file_adsp_nimble, file_adsp_illum, cols_filter):
	## variable setup
	cols_join = ['POS']
	cols_snv = ['CHR', 'POS', 'rsID', 'RefAllele', 'AltAllele', 'VTYPE']
	
	## process individual snv.summary.* files
	psp_df, psp_cnt = process_snv_summary_file(os.path.join(curr_dir, file_psp),
											   cohort='psp',
											   platform='nimblegen')
	adsp_n_df, adsp_n_cnt = process_snv_summary_file(os.path.join(curr_dir, file_adsp_nimble),
													 cohort='adsp',
													 platform='nimblegen')
	adsp_i_df, adsp_i_cnt = process_snv_summary_file(os.path.join(curr_dir, file_adsp_illum),
													 cohort='adsp',
													 platform='illumina')
	
	## extract & rename columns before merge
	_psp = psp_df[cols_snv+cols_filter].rename(columns={c:c+'_psp' for c in cols_filter})
	_adsp_n = adsp_n_df[cols_join+cols_filter].rename(columns={c:c+'_adsp_n' for c in cols_filter})
	_adsp_i = adsp_i_df[cols_join+cols_filter].rename(columns={c:c+'_adsp_i' for c in cols_filter})
	
	## outer join SNV columns & outer join
	join_df = _psp.merge(_adsp_n, on=cols_join, how='outer')\
				.merge(_adsp_i, on=cols_join, how='outer')
	
	## combine count DFs
	
	################################################################
	## @TODO - run with specific Pandas version -
	if pd.__version__.startswith('0.25.'):
		cnt_df = pd.concat([psp_cnt, adsp_n_cnt, adsp_i_cnt], axis=0, sort=False)
	else:
		cnt_df = pd.concat([psp_cnt, adsp_n_cnt, adsp_i_cnt], axis=0)
	################################################################
	
	return join_df, cnt_df
	



def count_filter_merged_psp_adsp(df, filter_target_col, filter_target_val, 
								 filter_pass_col, filter_pass_val):
	## set up boolean filters
	_bool_union = (df[filter_target_col+'_psp'] == filter_target_val)                         | (df[filter_target_col+'_adsp_n'] == filter_target_val)                         | (df[filter_target_col+'_adsp_i'] == filter_target_val)
	_bool_intersect = (df[filter_target_col+'_psp'] == filter_target_val)                         & (df[filter_target_col+'_adsp_n'] == filter_target_val)                         & (df[filter_target_col+'_adsp_i'] == filter_target_val)
	
	_bool_pass_filter = (df[filter_pass_col+'_psp'] == filter_pass_val)                         & (df[filter_pass_col+'_adsp_n'] == filter_pass_val)                         & (df[filter_pass_col+'_adsp_i'] == filter_pass_val)

	## count merged DF SNV subsets
	count_variables = ['in_target_intersect_pass_qc', 'in_target_intersect',
					   'in_target_union', 'total_in_file',
					   'exclude_off_target', 'exclude_intersect_fail_qc']
	_s = pd.Series(index=count_variables, data=0)
	_s.loc['total_in_file'] = df.shape[0]
	_s.loc['in_target_union'] = df[_bool_union].shape[0]
	_s.loc['in_target_intersect'] = df[_bool_intersect].shape[0]
	_s.loc['in_target_intersect_pass_qc'] = df[_bool_pass_filter].shape[0]
	_s.loc['exclude_off_target'] = int(_s.loc['total_in_file'])                                             - int(_s.loc['in_target_intersect'])
	_s.loc['exclude_intersect_fail_qc'] = int(_s.loc['in_target_intersect'])                                             - int(_s.loc['in_target_intersect_pass_qc'])
	cnt_df = _s.reset_index(drop=False, name='count').rename(columns={'index':'snv_subset'})
	cnt_df['cohort'] = 'Combined_dataset'
	
	## filter merged DF --> filtered SNV list
	cols_drop = [c for c in df.columns if (('_psp' in c) | ('_adsp' in c))]
	snv_df = df[_bool_pass_filter]                .drop(columns=cols_drop)                .sort_values(by='POS', ascending=True)                .reset_index(drop=True)

	return snv_df, cnt_df



def run_one_chromosome(chrom, dir_snv, dir_filt_count, dir_filt_snv, psp, adsp_nimble, adsp_illum, filter_target_col, filter_target_val, filter_pass_col, filter_pass_val, cols_filter=None):    
	print("\n", chrom)
	## setup variables
	if cols_filter is None:
		cols_filter = [filter_target_col, filter_pass_col]
		
	cwd = os.path.join(os.path.abspath(dir_snv), chrom)
	file_merge = os.path.join(cwd, 'psp_adsp.snv.merged_filters.tsv')
	file_count = os.path.join(dir_filt_count, 'psp_adsp.filtered.snv.qc_counts.' + chrom + '.tsv')
	file_filter_snv = os.path.join(dir_filt_snv, 'psp_adsp.filtered.snv.' + chrom + '.tsv')
	
	## merge 3 snv summary files
	join_df, cnt_file_df = merge_psp_adsp_snv_dataset(cwd, psp, adsp_nimble, adsp_illum, cols_filter)
	
	## use QC columns to count & filter SNVs
	snv_df, cnt_merge_df = count_filter_merged_psp_adsp(join_df, filter_target_col, filter_target_val,
														filter_pass_col, filter_pass_val)
	
	## concat & reorder SNV count DF columns
	cols_count_df = ['cohort', 'platform', 'snv_subset', 'count']
	
	################################################################
	## @TODO - run with specific Pandas version -
	if pd.__version__.startswith('0.25.'):
		cnt_df = pd.concat([cnt_merge_df, cnt_file_df], axis=0, sort=False)
	else:
		cnt_df = pd.concat([cnt_merge_df, cnt_file_df], axis=0)
	################################################################
	cnt_df = cnt_df[cols_count_df].reset_index(drop=True)
	cnt_df['count'] = cnt_df['count'].astype(int)
	
	
	## write filtered SNV list, SNV merged manifest, & SNV count output files
	join_df.to_csv(file_merge, header=True, index=False, sep='\t')
	snv_df.to_csv(file_filter_snv, header=True, index=False, sep='\t')
	cnt_df.to_csv(file_count, header=True, index=False, sep='\t')
	
	del join_df
	del snv_df
	return cnt_df.rename(columns={'count':int(chrom.replace('chr',''))})



def run_chrom_wrapper(curr_chrom):
	return run_one_chromosome(curr_chrom, dir_snv=DIR_SNV, dir_filt_count=DIR_FILTER_COUNT, dir_filt_snv=DIR_FILTER_SNV,
							  psp=PSP_NIMBLEGEN, adsp_nimble=ADSP_NIMBLEGEN, adsp_illum=ADSP_ILLUMINA,
								  filter_target_col=FILT_TARGET_COL, filter_target_val=FILT_TARGET_VAL,
								  filter_pass_col=FILT_QCPASS_COL, filter_pass_val=FILT_QCPASS_VAL,
								  cols_filter=COLS_FILTER)



def run_all_chrom(dir_snv, dir_filter_count, dir_filter_snv, test_chr=None):    
	## setup output directories
	pathlib.Path(dir_filter_count).mkdir(parents=True, exist_ok=True)
	pathlib.Path(dir_filter_snv).mkdir(parents=True, exist_ok=True)
	
	## get chromosome list
	if test_chr is None:
		_chr_dir = [_dir for _dir in os.listdir(dir_snv) if 'chr' in _dir]
	else:
		_chr_dir = test_chr ## subset of chrom for testing
	
	## @TODO: refactor!!!
	chr_count_df_list = [run_chrom_wrapper(_chrom) for _chrom in _chr_dir]
	
	## merge chromosome SNV counts
	idx_cnt = ['cohort', 'platform', 'snv_subset']
	for _df in chr_count_df_list:
		_df = _df.set_index(idx_cnt, inplace=True)
	
	
	################################################################
	## @TODO - run with specific Pandas version -
	if pd.__version__.startswith('0.25.'):
		chrom_cnt_merge_df = pd.concat(chr_count_df_list, axis=1, sort=False)
	else:
		chrom_cnt_merge_df = pd.concat(chr_count_df_list, axis=1)
	################################################################
	
	
	## sort chromosome columns
	_cols_sort = sorted(chrom_cnt_merge_df.columns.tolist())
	chrom_cnt_merge_df = chrom_cnt_merge_df[_cols_sort]
	chrom_cnt_merge_df.rename(columns={c:'chr'+str(c) for c in chrom_cnt_merge_df.columns}, inplace=True)
	
	## write merged chromosomes output file
	out_file_all_chrom = os.path.join(dir_filter_count,
									  'psp_adsp.filter_qc.snv_counts.all_chr.tsv')
	chrom_cnt_merge_df.to_csv(out_file_all_chrom, header=True, index=True, sep='\t')
	
	return chrom_cnt_merge_df




if __name__ == "__main__":
	_run_test = False
	
	## @TODO: PASS IN 2 DIR!!!
	
	#### Define File I/O variables
	DIR_SNV="../../data/qc_files/snv/"
	DIR_FILTER="../../data/qc_files/filtered/"
	DIR_FILTER_COUNT=os.path.join(DIR_FILTER, 'qc_filter_counts')
	DIR_FILTER_SNV=os.path.join(DIR_FILTER, 'eligible_snv')

	
	if len(sys.argv) > 1:
		if sys.argv[1] == 'test':
			run_test = True
	
	if _run_test:
		test_combined_count_df = run_all_chrom(dir_snv=DIR_SNV,
										  dir_filter_count=DIR_FILTER_COUNT,
										  dir_filter_snv=DIR_FILTER_SNV,
										  test_chr=['chr4', 'chr21'])
	
		print(test_combined_count_df)

	else:
		#### Run on ALL chromosomes
		combined_count_df = run_all_chrom(dir_snv=DIR_SNV, dir_filter_count=DIR_FILTER_COUNT, dir_filter_snv=DIR_FILTER_SNV)
	
		print(combined_count_df)
	
	




