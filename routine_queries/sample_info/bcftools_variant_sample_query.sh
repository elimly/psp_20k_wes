#!/bin/bash

VCF_FILE=$1
TARGET_FILE=$2
FILE_NAME=$3
HOM_REF_SAMPLES=$4

VAR_VCF_FILE=${FILE_NAME}.vcf
VAR_NORM_VCF_FILE=${FILE_NAME}.vcf.norm.vcf
SAMPLE_FILE=${FILE_NAME}.vcf.sample_info.txt

echo -e "\n$0 started...\n"

## 1. Extract subset of variants
echo -e "\nstep 1: extracting variants..."
bcftools view --no-version --targets-file $TARGET_FILE --output-type v --output-file $VAR_VCF_FILE $VCF_FILE


## 2. Normalize extracted VCF
echo -e "\nstep 2: normalizing extracted VCF file..."
bcftools norm -m-any $VAR_VCF_FILE -Ov > $VAR_NORM_VCF_FILE


## 3. get Genotype Counts & Sample Lists per variant 
echo -e "\nstep 3: generating genotype counts & sample lists..."

# Code adapted from https://www.biostars.org/p/298361/ - credit Kevin Blighe.
# 1. Outputs the first few columns from the VCF/BCF.
# 2. Three BCFtools query commands: tabulate the number of samples having each variant type. This will work for phased and/or unphased variants. The output is the 3 columns named numHet, numHomAlt, numHomRef
# 3. Three BCFtools view commands: look through the file again, saving sample names into an array called 'header', and then printing the indices of the array where a particular field (i.e. sample) has a particular type of variant. This is repeated for: a, heterozygous (Het_samples), b, homozygous (alt) (HomAlt_samples), and c, homozygouse (ref) (HomRef_samples).
get_samples_hom_ref(){
  paste <(bcftools view $VAR_NORM_VCF_FILE |\
      awk -F"\t" 'BEGIN {print "CHROM\tPOS\tID\tREF\tALT"} \
        !/^#/ {print $1"\t"$2"\t"$3"\t"$4"\t"$5}') \
      \
    <(bcftools query -f '[\t%SAMPLE=%GT]\n' $VAR_NORM_VCF_FILE |\
      awk 'BEGIN {print "numHet"} {print gsub(/0\|1|1\|0|0\/1|1\/0/, "")}') \
      \
    <(bcftools query -f '[\t%SAMPLE=%GT]\n' $VAR_NORM_VCF_FILE |\
      awk 'BEGIN {print "numHomAlt"} {print gsub(/1\|1|1\/1/, "")}') \
      \
    <(bcftools query -f '[\t%SAMPLE=%GT]\n' $VAR_NORM_VCF_FILE |\
      awk 'BEGIN {print "numHomRef"} {print gsub(/0\|0|0\/0/, "")}') \
      \
    <(bcftools view $VAR_NORM_VCF_FILE | awk -F"\t" '/^#CHROM/ {split($0, header, "\t"); print "Het_samples"} \
      !/^#CHROM/ {for (i=10; i<=NF; i++) {if (gsub(/0\|1|1\|0|0\/1|1\/0/, "", $(i))==1) {printf header[i]", "}; if (i==NF) {printf "\n"}}}') \
      \
    <(bcftools view $VAR_NORM_VCF_FILE | awk -F"\t" '/^#CHROM/ {split($0, header, "\t"); print "HomAlt_samples"} \
      !/^#CHROM/ {for (i=10; i<=NF; i++) {if (gsub(/1\|1|1\/1/, "", $(i))==1) {printf header[i]", "}; if (i==NF) {printf "\n"}}}') \
      \
    <(bcftools view $VAR_NORM_VCF_FILE | awk -F"\t" '/^#CHROM/ {split($0, header, "\t"); print "HomRef_samples"} \
      !/^#CHROM/ {for (i=10; i<=NF; i++) {if (gsub(/0\|0|0\/0/,"", $(i))==1) {printf header[i]", "}; if (i==NF) {printf "\n"}}}') \
      \
    | sed 's/,\t/\t/g' | sed 's/,$//g' > $SAMPLE_FILE
}

get_samples_no_hom_ref(){
  paste <(bcftools view $VAR_NORM_VCF_FILE |\
      awk -F"\t" 'BEGIN {print "CHROM\tPOS\tID\tREF\tALT"} \
        !/^#/ {print $1"\t"$2"\t"$3"\t"$4"\t"$5}') \
      \
    <(bcftools query -f '[\t%SAMPLE=%GT]\n' $VAR_NORM_VCF_FILE |\
      awk 'BEGIN {print "numHet"} {print gsub(/0\|1|1\|0|0\/1|1\/0/, "")}') \
      \
    <(bcftools query -f '[\t%SAMPLE=%GT]\n' $VAR_NORM_VCF_FILE |\
      awk 'BEGIN {print "numHomAlt"} {print gsub(/1\|1|1\/1/, "")}') \
      \
    <(bcftools query -f '[\t%SAMPLE=%GT]\n' $VAR_NORM_VCF_FILE |\
      awk 'BEGIN {print "numHomRef"} {print gsub(/0\|0|0\/0/, "")}') \
      \
    <(bcftools view $VAR_NORM_VCF_FILE | awk -F"\t" '/^#CHROM/ {split($0, header, "\t"); print "Het_samples"} \
      !/^#CHROM/ {for (i=10; i<=NF; i++) {if (gsub(/0\|1|1\|0|0\/1|1\/0/, "", $(i))==1) {printf header[i]", "}; if (i==NF) {printf "\n"}}}') \
      \
    <(bcftools view $VAR_NORM_VCF_FILE | awk -F"\t" '/^#CHROM/ {split($0, header, "\t"); print "HomAlt_samples"} \
      !/^#CHROM/ {for (i=10; i<=NF; i++) {if (gsub(/1\|1|1\/1/, "", $(i))==1) {printf header[i]", "}; if (i==NF) {printf "\n"}}}') \
      \
    | sed 's/,\t/\t/g' | sed 's/,$//g' > $SAMPLE_FILE
}

if [ "$HOM_REF_SAMPLES" == "true" ];
then
  echo -e "\tinclude list of Homozygous REF samples \n"
  get_samples_hom_ref
else
  echo -e "\tdo NOT include Homozygous REF sample list \n"
  get_samples_no_hom_ref
fi  

echo -e "\n\n$0 complete. Goodbye.\n\n"
