#!/usr/bin/env python
# coding: utf-8

"""
!!! NOTE: requires Pandas >=25.* !!! 
(due to explode cmd in line 20 -- explode introduced in Pandas 25.1)
"""

import pandas as pd
import argparse, os

def explode_sample_lists(df):
    cols_samp = [c for c in df.columns if '_samples' in c]
    cols_var = [c for c in df.columns if c not in cols_samp]
    frames = []
    for gt in cols_samp:
        _gt_df = df.loc[df[gt].notnull(), cols_var+[gt]]    
        if _gt_df.shape[0] > 0:
            _gt_df['sample_ID'] = _gt_df[gt].apply(lambda x: x.split(', '))
            _gt_df = _gt_df.explode('sample_ID')
            _gt_df['sample_genotype'] = gt.replace('_samples','')
            _gt_df.drop(columns=[gt], inplace=True)
            frames.append(_gt_df)
    
    samp_df = pd.concat(frames, ignore_index=True)
    samp_df['chr'] = samp_df['CHROM'].copy().str.replace('chr','').astype(int)
    samp_df = samp_df.sort_values(['chr', 'POS', 'sample_genotype', 'sample_ID'])\
                    .reset_index(drop=True)\
                    .drop(columns=['chr'])
    return samp_df

def write_files(input_fname, samp_df, var_samp_df):
    output_samples_excel = input_fname.rsplit('.sample_')[0] + '.samples.xlsx'
    output_samp_txt = input_fname.rsplit('.sample_')[0] + '.sample_list.txt'

    ## combined excel file
    with pd.ExcelWriter(output_samples_excel) as writer: 
        samp_df.to_excel(writer, sheet_name='sample list', header=True, index=False)
        var_samp_df.to_excel(writer, sheet_name='variant sample info', header=True, index=False)
    ## long format sample lists excel & txt file
    samp_df.to_csv(output_samp_txt, header=True, index=False, sep='\t')

def convert_bcftools_sample_data(input_sample_file):
    input_file_path = os.path.abspath(input_sample_file)

    ## read in input file
    df = pd.read_csv(input_file_path, sep='\t')
    
    ## explode to long format sample list
    samp_df = explode_sample_lists(df)
    var_samp_df = df.copy().fillna('')
    
    ## write output files
    write_files(input_file_path, samp_df, var_samp_df)



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--file')
    
    ## parse input bcftools sample_data.txt file
    pargs = parser.parse_args()
    bcftools_sample_file = pargs.file

    ## run program
    convert_bcftools_sample_data(bcftools_sample_file)

