#!/bin/bash

VCF_FILE="batch704.g.vcf.gz"
TARGET_FILE="01_LRP10_PSP_sample_query_10152019_bcftools_targets.txt"
FILE_NAME="01_LRP10_PSP_sample_query_10152019"
HOM_REF_SAMPLES="false"

./bcftools_variant_sample_query.sh "$VCF_FILE" "$TARGET_FILE" "$FILE_NAME" "$HOM_REF_SAMPLES"
