#!/bin/bash

## REQUIRED: path to Rscript.exe
RSCRIPT="/usr/local/bin/Rscript"

## REQUIRED: absolute path to ideogram_density_plot_3way.R (no trailing /)
DIR_SCRIPT=$(cd $(dirname "$1") && pwd -P)/$(basename "$1")$(basename $(dirname "$0")) ## use if this driver script is in the same dir 

## optional: absolute path to input file directory (use if 3 files in same dir)
DIR_FILE="${DIR_SCRIPT%%"/."}""/../../data/02_post_qc/singletons/bed_files"


###############################################################################
#### REQUIRED arguments 
###############################################################################
## specify 3 files to plot 
#### format option #1: .bed no header 
#### format option #2: tab-separated with 'CHR' | 'CHROM' & 'POS' columns
FILE1=${DIR_FILE}/"plot_singleton_unique.psp.bed"
FILE2=${DIR_FILE}/"plot_singleton_unique.adsp_nimblegen.bed"
FILE3=${DIR_FILE}/"plot_singleton_unique.adsp_illumina.bed"

## 3 labels for plot - str: separate labels by single ',' 
LABEL_STR="PSP,ADPS n,ADSP i"	## labels may contain spaces


###############################################################################
#### OPTIONAL arguments -- default values will be supplied if not specified
####		** add / remove from Rscript args as necessary **
###############################################################################
## output directory (no trailing /)
OUT_DIR="~/repos/bitbucket/psp_hg38_wes_dev/data/02_post_qc/singletons/plots"
## output plot file name
OUT_NAME="test_PSP_ADSP_Singletons-unique_density_plot.pdf"
## plot title
TITLE="SINGLETONS unique to cohort - capture kit subset"
## density window size
WINDOW_SIZE=500000
## subset of chromosomes to plot - str: separate labels by single ',' 
# CHROM_STR="chr21" 	## default: "autosomal"

#-----------------------------------------------------------------------------#

PLOT_SCRIPT="${DIR_SCRIPT}"/"ideogram_density_plot_3way.R"

echo "before script call"

$RSCRIPT $PLOT_SCRIPT \
	FILE1="${FILE1}" FILE2="${FILE2}" FILE3="${FILE3}"\
	LABEL_STR="${LABEL_STR}" \
	TITLE="${TITLE}"  \
	OUT_DIR="${OUT_DIR}" \
	OUT_NAME="${OUT_NAME}" \
	WINDOW_SIZE=${WINDOW_SIZE}

echo "after script call"






