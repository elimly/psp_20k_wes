#!/bin/bash

##----- paths & file names on OLD-METHOD-C:34.201.102.64  -----##

## directory with original 20k hg38 QC companion snv files:
DIR_ORIG=/mnt/adsp/ftpsite/ftp_UTH_TMC/gcad.r2.wes.qc.pvcf.20158.2019.1107/companion_files/snv

## PSP hg38 WES analysis workspace directory
DIR_PSP_WS=/mnt/adsp/users/psp_hg38_wes/post_QC_filtering/orig_20k_hg38_WES_links/snv

## PSP & ADSP snv.summary file names
PSP_NIMBLEGEN="summary.snv.PSP-Nimblegen_VCRome_V2.1.tsv"
ADSP_ILLUMINA="summary.snv.ADSP_Discovery-Illumina_Rapid_Capture_Exome_(ICE)_kit.tsv"
ADSP_NIMBLEGEN="summary.snv.ADSP_Discovery-Nimblegen_VCRome_V2.1.tsv"



##----- setup PSP workspace QC companion snv.summary files -----##

## for each CHROM dir, make new dir & create symbolic links to original files
for DIR in $DIR_ORIG/chr*/ ; do 
	## create new directory
	DIR_CHR=$(basename "$DIR")
	echo $DIR_PSP_WS/$DIR_CHR
	mkdir -p $DIR_PSP_WS/$DIR_CHR
	## create symbolic links
	ln -s $DIR$PSP_NIMBLEGEN $DIR_PSP_WS/$DIR_CHR/$PSP_NIMBLEGEN
	ln -s $DIR$ADSP_ILLUMINA $DIR_PSP_WS/$DIR_CHR/$ADSP_ILLUMINA
	ln -s $DIR$ADSP_NIMBLEGEN $DIR_PSP_WS/$DIR_CHR/$ADSP_NIMBLEGEN
done

