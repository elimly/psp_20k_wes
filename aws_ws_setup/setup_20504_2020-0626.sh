#!/bin/bash

##----- 20kWES paths & file names on OLD-METHOD-C:34.201.102.64  -----##

## directory with original 20k hg38 QC companion snv files:
DIR_20k="/mnt/adsp/ftpsite/ftp_UTH_TMC/gcad.r2.wes.qc.pvcf.20504.2020.0626"
DIR_COMPANION="companion_files"
DIR_COMPANION_SNV="companion_files/snv"
DIR_PHENO="phenotypes_adsp_umbrella_ng00067.v3"
DIR_VCF="pVCF"
DIR_QC="QCmetrics"
DIR_TARGET="target_regions"


## companion_files/snv 
FILES_SNV=(
	"gcad.qc.wes.allchr.20504.GATK.2020.06.26.biallelic.ADSP_Discovery-Illumina_Rapid_Capture_Exome_(ICE)_kit.summary.tar.gz"
	"gcad.qc.wes.allchr.20504.GATK.2020.06.26.biallelic.ADSP_Discovery-Nimblegen_VCRome_V2.1.summary.tar.gz"
	"gcad.qc.wes.allchr.20504.GATK.2020.06.26.biallelic.PSP-Nimblegen_VCRome_V2.1.summary.tar.gz"
	)

## phenotype files
FILES_PHENO=(
	"20kwes_studydescriptions.docx"
	"ADSPCaseControlPhenotypes_DD_2020.06.26.xlsx"
	"ADSPCaseControlPhenotypes_DS_2020.06.26_ALL.xlsx"
	"adsp_cohort-specific_notes.docx"
	"ADSP_Discovery_WES_vcpa1.1_dropped.xlsx"
	"README_PhenotypesADSPUmbrella_ng00067.v3.docx"
	"SampleManifest_DD_2020.06.26.xlsx"
	"SampleManifest_DS_2020.06.26_ALL.xlsx"
	"SubjectNotConsented_2020.06.26.xlsx"
	"PSPCBDPhenotypes_DD_2019.11.07.v2.xlsx"
	"PSPCBDPhenotypes_DS_2019.11.07.v2_ALL.xlsx"
	)

## target regions files
FILES_TARGET=(
	"gcad.wes.20650.VCPA1.1.2019.11.01.targetregions.README.txt"
	"Illumina_Rapid_Capture_Exome_ICE_kit.bed"
	"Nimblegen_VCRome_V2.1.bed"
	)


##----- workspace paths on OLD-METHOD-C:34.201.102.64  -----##

## hg38 WES analysis workspace directories
DIR_WS="/mnt/adsp/users/psp_hg38_wes/data"
DIR_WS_00="${DIR_WS}"/20kWES_00_links_to_orig
DIR_WS_01="${DIR_WS}"/20kWES_01_gcad




###############################################################################
##### set up workspace
###############################################################################


## companion_files: *all files
echo "companion_files"
mkdir -p "${DIR_WS_00}"/"${DIR_COMPANION}"
mkdir -p "${DIR_WS_01}"/"${DIR_COMPANION}"

## copy all companion files
for F in "${DIR_20k}"/"${DIR_COMPANION}"/*.* ; do
	echo $(basename "$F")

	## copy & create symbolic link to original file
	ln -s "$F" "${DIR_WS_00}"/"${DIR_COMPANION}"/$(basename "$F")
	cp "$F" "${DIR_WS_01}"/"${DIR_COMPANION}"/$(basename "$F")
done


## companion_files/snv: FILES_SNV list of files
echo "companion_files_SNV"
mkdir -p "${DIR_WS_00}"/"${DIR_COMPANION_SNV}"
mkdir -p "${DIR_WS_01}"/"${DIR_COMPANION_SNV}"

## copy list of snv summary files
for F_NAME in "${FILES_SNV[@]}" ; do
	echo $F_NAME
	F="${DIR_20k}"/"${DIR_COMPANION_SNV}"/"${F_NAME}"

	## copy & create symbolic link to original file
	ln -s "$F" "${DIR_WS_00}"/"${DIR_COMPANION_SNV}"/"${F_NAME}"
	cp "$F" "${DIR_WS_01}"/"${DIR_COMPANION_SNV}"/"${F_NAME}"
	tar -xvf "${DIR_WS_01}"/"${DIR_COMPANION_SNV}"/"${F_NAME}"
done


## phenotype files - both: FILES_PHENO
echo "Pheno"
mkdir -p "${DIR_WS_00}"/"${DIR_PHENO}"
mkdir -p "${DIR_WS_01}"/"${DIR_PHENO}"

## copy list of pheno files
for F_NAME in "${FILES_PHENO[@]}" ; do
	echo $F_NAME
	F="${DIR_20k}"/"${DIR_PHENO}"/"${F_NAME}"

	## copy & create symbolic link to original file
	ln -s "$F" "${DIR_WS_00}"/"${DIR_PHENO}"/"${F_NAME}"
	cp "$F" "${DIR_WS_01}"/"${DIR_PHENO}"/"${F_NAME}"
done


## pVCF - DIR_WS_00 only: *all files
echo "pVCF"
mkdir -p "${DIR_WS_00}"/"${DIR_VCF}"

## make symlink all to orig 20kWES pVCF
for F in "${DIR_20k}"/"${DIR_VCF}"/*.* ; do
	echo $(basename "$F")

	## copy & create symbolic link to original file
	ln -s "$F" "${DIR_WS_00}"/"${DIR_VCF}"/$(basename "$F")
done


## QCmetrics: *all files
echo "QCmetrics"
mkdir -p "${DIR_WS_00}"/"${DIR_QC}"
mkdir -p "${DIR_WS_01}"/"${DIR_QC}"

## copy all QC metric files
for F in "${DIR_20k}"/"${DIR_QC}"/*.* ; do
	echo $(basename "$F")

	## copy & create symbolic link to original file
	ln -s "$F" "${DIR_WS_00}"/"${DIR_QC}"/$(basename "$F")
	cp "$F" "${DIR_WS_01}"/"${DIR_QC}"/$(basename "$F")
done



## target regions files: FILES_TARGET list of files
echo "target_regions"
mkdir -p "${DIR_WS_00}"/"${DIR_TARGET}"
mkdir -p "${DIR_WS_01}"/"${DIR_TARGET}"

## copy list of target regions files
for F_NAME in "${FILES_TARGET[@]}" ; do
	echo $F_NAME
	F="${DIR_20k}"/"${DIR_TARGET}"/"${F_NAME}"

	## copy & create symbolic link to original file
	ln -s "$F" "${DIR_WS_00}"/"${DIR_TARGET}"/"${F_NAME}"
	cp "$F" "${DIR_WS_01}"/"${DIR_TARGET}"/"${F_NAME}"
done


echo "file setup complete. goodbye."
