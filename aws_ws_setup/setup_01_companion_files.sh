#!/bin/bash

##----- paths & file names on OLD-METHOD-C:34.201.102.64  -----##

## directory with original 20k hg38 QC companion snv files:
DIR_ORIG=/mnt/adsp/ftpsite/ftp_UTH_TMC/gcad.r2.wes.qc.pvcf.20158.2019.1107/companion_files

## PSP hg38 WES analysis workspace directories
DIR_PSP_DATA=/mnt/adsp/users/psp_hg38_wes/data/01_qc_output
DIR_PSP_LINK=/mnt/adsp/users/psp_hg38_wes/data/00_links_to_orig_20k_WES

## PSP & ADSP snv.summary file names
PSP_NIMBLEGEN="summary.snv.PSP-Nimblegen_VCRome_V2.1.tsv"
ADSP_ILLUMINA="summary.snv.ADSP_Discovery-Illumina_Rapid_Capture_Exome_(ICE)_kit.tsv"
ADSP_NIMBLEGEN="summary.snv.ADSP_Discovery-Nimblegen_VCRome_V2.1.tsv"



##----- setup PSP workspace QC companion snv.summary files -----##

mkdir -p $DIR_PSP_DATA/companion_files/snv
mkdir -p $DIR_PSP_LINK/companion_files/snv

## copy all non-SNV files
for F in $DIR_ORIG/*.* ; do
	echo $(basename "$F")

	## copy & create symbolic link to original file
	ln -s "$F" $DIR_PSP_LINK/companion_files/$(basename "$F")
	cp "$F" $DIR_PSP_DATA/companion_files/$(basename "$F")
done

## for each CHROM dir, make new dir & create symbolic links to original files
for DIR in $DIR_ORIG/snv/chr*/ ; do 
	DIR_CHR=$(basename "$DIR")
	echo $DIR_CHR

	## create new chr directories
	mkdir -p $DIR_PSP_LINK/companion_files/snv/$DIR_CHR
	mkdir -p $DIR_PSP_DATA/companion_files/snv/$DIR_CHR

	## create symbolic links
	ln -s $DIR$PSP_NIMBLEGEN $DIR_PSP_LINK/companion_files/snv/$DIR_CHR/$PSP_NIMBLEGEN
	ln -s $DIR$ADSP_ILLUMINA $DIR_PSP_LINK/companion_files/snv/$DIR_CHR/$ADSP_ILLUMINA
	ln -s $DIR$ADSP_NIMBLEGEN $DIR_PSP_LINK/companion_files/snv/$DIR_CHR/$ADSP_NIMBLEGEN

	## copy files
	cp $DIR$PSP_NIMBLEGEN $DIR_PSP_DATA/companion_files/snv/$DIR_CHR/$PSP_NIMBLEGEN
	cp $DIR$ADSP_ILLUMINA $DIR_PSP_DATA/companion_files/snv/$DIR_CHR/$ADSP_ILLUMINA
	cp $DIR$ADSP_NIMBLEGEN $DIR_PSP_DATA/companion_files/snv/$DIR_CHR/$ADSP_NIMBLEGEN	
done



