#!/bin/bash

## eigenstrat00_1000G_config-PSP_ADSP.20504.sh

###############################################################################
##### User supplied variables 
###############################################################################

## specify workspace directory
DIR_WS="/mnt/adsp/users/psp_hg38_wes/data/PLINK"
DIR_00_WS="/mnt/adsp/users/psp_hg38_wes/data/PLINK/eigenstrat_1000G"

## specify Cohort - specific values
DIR_COHORT="/mnt/adsp/users/psp_hg38_wes/data/PLINK/filter1_extract_from_20kWES"
IN_PLINK_NAME_COHORT="psp_adsp_filter1_merge"
COHORT_NAME="psp_adsp"
COHORT_ABV="psp"
COHORT_CASE="psp"
COHORT_CTRL="ADSP ctrl"

## specify 1000 Genomes - specific values
DIR_1000G="/mnt/adsp/users/psp_hg38_wes/1000G_p3_hg38/plink_files"
IN_PLINK_NAME_1000G="1000G_p3_hg38_merge"

## specify software & environment settings
PLINKCMD="/usr/local/bin/plink-1.90b6.9 "
AWK="awk"

## Eigenstrat SmartPCA settings
EIGENSTRAT="/home/elimly/bin/EIG-6.1.4/bin"
SMARTPCACONVERT="/home/elimly/bin/EIG-6.1.4/bin/convertf "
SMARTPCA="/home/elimly/bin/EIG-6.1.4/bin/smartpca.perl"

# EIGENSTRAT="/usr/local/bin/eigenstrat/bin"
# SMARTPCACONVERT="/usr/local/bin/eigenstrat/bin/convertf "
# SMARTPCA="/usr/local/bin/eigenstrat/bin/smartpca.perl"

NSAMPLEMARKER4=20000 ## sample markers from LD pruned list
SMARTPCAMAXITER=0  ## max # smartpca outlier removal iterations. Default is 5. 
					## To turn off outlier removal, set this parameter to 0.
SMARTPCASIGMATH=6  ## sigma for smartpca outlier detection.  Default is 6.  
                   ## Change to some large number to include everyone


## R settings
R="/usr/local/bin/R --no-save "
RSCRIPT_SAMPLE_PRUNED=/mnt/adsp/users/psp_hg38_wes/src/PLINK/03_eigenstrat_GWAS/r_ldprune_sample_autosomal_snps.R




###############################################################################
##### Derived variables 
###############################################################################

DIR_01_SNPLIST="${DIR_00_WS}"/01_"${COHORT_ABV}"_1000G_snplist
DIR_02_COMMON_PRUNE="${DIR_00_WS}"/02_common_pruned
DIR_03_MERGE="${DIR_00_WS}"/03_merge
DIR_04_MERGE_PRUNE="${DIR_00_WS}"/04_merge_pruned
DIR_05_PCA="${DIR_00_WS}"/05_pca_1000G ##TODO: change to 05_pca_1000G
# DIR_06_PCA="${DIR_00_WS}"/06_pca_case_ctrl
DIR_06_PCA="${DIR_WS}"/eigenstrat_case_ctrl

COHORT_PREFIX1="${DIR_COHORT}"/"${IN_PLINK_NAME_COHORT}"
COHORT_PREFIX2="${DIR_COHORT}"/pruned/"${IN_PLINK_NAME_COHORT}"_pruned
COHORT_PREFIX3="${DIR_02_COMMON_PRUNE}"/"${IN_PLINK_NAME_COHORT}"_common_pruned

G1000_PREFIX1="${DIR_1000G}"/"${IN_PLINK_NAME_1000G}"
G1000_PREFIX2="${DIR_1000G}"/pruned/1000G_p3_hg38_pruned
G1000_PREFIX3="${DIR_02_COMMON_PRUNE}"/1000G_common_pruned

COHORT_SNPLIST="${DIR_01_SNPLIST}"/"${IN_PLINK_NAME_COHORT}"_pruned_snplist.txt
G1000_SNPLIST="${DIR_01_SNPLIST}"/1000G_p3_hg38_pruned_snplist.txt
COMMON_SNPLIST="${DIR_01_SNPLIST}"/common_pruned_snplist.txt

MERGE_NAME="${COHORT_ABV}""_1000G_merge"
MERGE_PREFIX1="${DIR_03_MERGE}"/"${MERGE_NAME}"
MERGE_PREFIX2="${DIR_04_MERGE_PRUNE}"/"${MERGE_NAME}"_pruned

##TODO: change to "${COHORT_NAME}""-1000G_PCA"
PCA_NAME="${COHORT_ABV}""_1000G_ld_pruned_20k" 
PCA_PREFIX="${DIR_05_PCA}"/"${PCA_NAME}"

PCA_NAME2="${COHORT_ABV}""_ld_pruned_20k_outliers_removed"
PCA_PREFIX2="${DIR_06_PCA}"/"${PCA_NAME2}"

DIR_OUT_PCA1="${DIR_05_PCA}"/post_PCA
DIR_OUT_PCA2="${DIR_06_PCA}"/post_PCA


OUTLIER="${DIR_OUT_PCA1}"/"${PCA_NAME}""-nonNHW_outliers_visual.txt" 



