#!/bin/bash

source eigenstrat00_1000G_config-PSP_ADSP.20504.sh

## MUST ADD smartpca to $PATH or will NOT work!
PATH=$EIGENSTRAT:$PATH

# mkdir -p $DIR_OUT_PCA2
mkdir -p $DIR_06_PCA $DIR_OUT_PCA2

echo "input plink:       "$COHORT_PREFIX3
echo "extract snps:      "$MERGE_PREFIX2
echo "remove outliers:   "$OUTLIER 
echo "pca input plink:   "$PCA_PREFIX2 



## extract 20k SNPs sampled from LD pruned PLINK dataset:
$PLINKCMD --bfile $COHORT_PREFIX3 \
		--extract $MERGE_PREFIX2.prune.in.autosomal.sample.id \
		--remove $OUTLIER \
		--recode --make-bed \
		--out $PCA_PREFIX2 


## Configure settings & convert files for SmartPCA input
echo 'genotypename:    '$PCA_PREFIX2'.bed '    > $PCA_PREFIX2.smartpca_convert.par
echo 'snpname:         '$PCA_PREFIX2'.bim '    >> $PCA_PREFIX2.smartpca_convert.par
echo 'indivname:       '$PCA_PREFIX2'.fam'     >> $PCA_PREFIX2.smartpca_convert.par
echo 'outputformat:    PACKEDPED'              >> $PCA_PREFIX2.smartpca_convert.par
echo 'genotypeoutname: '$PCA_PREFIX2'.a.bed'     >> $PCA_PREFIX2.smartpca_convert.par
echo 'snpoutname:      '$PCA_PREFIX2'.a.pedsnp'  >> $PCA_PREFIX2.smartpca_convert.par
echo 'indivoutname:    '$PCA_PREFIX2'.a.pedind'  >> $PCA_PREFIX2.smartpca_convert.par
echo 'familynames:     NO'                     >> $PCA_PREFIX2.smartpca_convert.par

$SMARTPCACONVERT -p $PCA_PREFIX2.smartpca_convert.par


## Run SmartPCA
$SMARTPCA \
		-a ${PCA_PREFIX2}.a.pedsnp \
		-b ${PCA_PREFIX2}.a.pedind \
		-i ${PCA_PREFIX2}.a.bed \
		-e ${PCA_PREFIX2}.a.eval \
		-l ${PCA_PREFIX2}.a.pcalog \
		-o ${PCA_PREFIX2}.a.pca \
		-p ${PCA_PREFIX2}.a.pca.par \
		-s $SMARTPCASIGMATH \
		-t 10


echo " ....... SmartPCA complete. Goodbye."
