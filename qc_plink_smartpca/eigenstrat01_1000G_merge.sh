#!/bin/bash

## eigenstrat01_1000G_merge.sh

## *NOTE* requires 10G memory (1000G large file size)
## To Run: qsub -l h_vmem=10G ./eigenstrat01_1000G_merge.sh 

source eigenstrat00_1000G_config.sh

mkdir -p $DIR_00_WS $DIR_01_SNPLIST $DIR_02_COMMON_PRUNE $DIR_03_MERGE $DIR_04_MERGE_PRUNE $DIR_05_PCA
mkdir -p "${DIR_COHORT}"/pruned "${DIR_1000G}"/pruned


#### Part 1:  LD prune PSP & 1000G & generate SNP lists 
## generate LD pruned SNP lists
$PLINKCMD --bfile $COHORT_PREFIX1 --indep-pairwise 50 5 0.2  --out $COHORT_PREFIX2
$PLINKCMD --bfile $G1000_PREFIX1 --indep-pairwise 50 5 0.2  --out $G1000_PREFIX2

## copy SNP lists to workspace
cp "${COHORT_PREFIX2}".prune.in "${COHORT_SNPLIST}"
cp "${G1000_PREFIX2}".prune.in "${G1000_SNPLIST}"


#### Part 2: intersect SNP lists -> identify common LD pruned SNPs  
comm -12 <(sort "${COHORT_SNPLIST}") <(sort "${G1000_SNPLIST}") > "${COMMON_SNPLIST}"

echo wc -l $DIR_01_SNPLIST/*snplist.txt


#### Part 3: extract common pruned SNPs
$PLINKCMD --bfile $COHORT_PREFIX1 --extract $COMMON_SNPLIST --recode --make-bed --out $COHORT_PREFIX3 
$PLINKCMD --bfile $G1000_PREFIX1 --extract $COMMON_SNPLIST --recode --make-bed --out $G1000_PREFIX3 


#### Part 4: merge extracted common pruned SNPs
## merge -- round 1
$PLINKCMD --bfile $COHORT_PREFIX3 --bmerge $G1000_PREFIX3.bed $G1000_PREFIX3.bim $G1000_PREFIX3.fam --make-bed --out $MERGE_PREFIX1

## exclude multi allelic SNPs
$PLINKCMD --bfile "${COHORT_PREFIX3}"  --exclude "${MERGE_PREFIX1}"-merge.missnp --make-bed --out "${COHORT_PREFIX3}"_nomiss
$PLINKCMD --bfile "${G1000_PREFIX3}"  --exclude "${MERGE_PREFIX1}"-merge.missnp --make-bed --out "${G1000_PREFIX3}"_nomiss

## merge -- round 2: no miss
$PLINKCMD --bfile "${COHORT_PREFIX3}"_nomiss --bmerge "${G1000_PREFIX3}"_nomiss --make-bed --out $MERGE_PREFIX1


#### Part 5: LD prune merged PLINK set  
$PLINKCMD --bfile $MERGE_PREFIX1 --indep-pairwise 50 5 0.2 --allow-no-sex --out $MERGE_PREFIX2 --write-snplist 


### Part 6: sample & extract 20k SNP markers for PCA 
# sample 20k SNPs sampled from LD prune.in:
$R PRUNEPREFIX=$MERGE_PREFIX2 INBIM=$MERGE_PREFIX1.bim NSAMPLE=$NSAMPLEMARKER4 < $RSCRIPT_SAMPLE_PRUNED

echo 'Number of autosomal markers after LD pruning: '`wc -l $MERGE_PREFIX2.prune.in.autosomal.id`

## extract 20k SNPs sampled from LD pruned PLINK dataset:
$PLINKCMD --bfile $MERGE_PREFIX1 --extract $MERGE_PREFIX2.prune.in.autosomal.sample.id --recode --make-bed --out $PCA_PREFIX 

