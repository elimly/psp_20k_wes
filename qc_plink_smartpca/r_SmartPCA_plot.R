
#------------------------------------------#
##############################################################
####### arguments
##############################################################

########## required
####  1 = pca.evec file name
####  2 = .fam file name
####  3 = cohort as string
####  4 = output path

########## optional
####  1 = case label as string
####  2 = control label as string

#------------------------------------------#

##############################################################
####### parse arguments
##############################################################

args <- commandArgs(trailingOnly = TRUE)
arglist <- list()
for (i in strsplit(args, "={1,}")) {
  arglist[[i[1]]]=i[2]
}

#### parse required args
evecfile = arglist$PCA_EVEC
famfile = arglist$PCA_FAM
cohort=arglist$COHORT
prefixOut=arglist$OUTPUT_DIR

#### parse  optional args
if(!any(names(arglist) %in% "COHORT_CASE")) {caseLabel = "Case"
} else {caseLabel = arglist$COHORT_CASE}
if(!any(names(arglist) %in% "COHORT_CTRL")) {ctrlLabel = "Ctrl"
} else {ctrlLabel = arglist$COHORT_CTRL}


##############################################################
####### set up File I/O variables
##############################################################

file_evec_df <- paste(prefixOut, "/", cohort, "_PCA_evec.txt", sep="")
file_plot_df <- paste(prefixOut, "/", cohort, "_PCA_plot_data.txt", sep="")
file_plot_pdf <- paste(prefixOut, "/", cohort, "_PCA_plot.pdf", sep='')


##############################################################
####### Read in files --> generate vectors & dataframes
##############################################################


evecOrig=read.table(evecfile,as.is=T,header=F)
fam=read.table(famfile,as.is=T)

#### convert evec file into PC dataframe 'a':
evec=evecOrig[,2:11]
idlist=t(sapply(evecOrig[,1],function(x){strsplit(x,":")[[1]]}))
colnames(evec)=sprintf("PC%02d",1:10)
a=data.frame(FID=idlist[,1],IID=idlist[,2],evec,stringsAsFactors=F)

#### extract from original evec file --> vector 'p'
p=as.double(strsplit(readLines(evecfile,1)," +")[[1]][3:12])

#### extract case / control status from original evec dataframe 'evecOrig'
dx_df = data.frame(IID=idlist[,2], DX=evecOrig[,12],stringsAsFactors=F)
dx_df$DX <- gsub("Control", "CTRL", dx_df$DX)
dx_df$DX <- gsub("Case", "CASE", dx_df$DX)


#### determine which row indices are Case & Control subjects
caseidx=which(a$IID %in% fam[which(fam$V6=="2"),]$V2)
ctrlidx=which((a$IID %in% fam[which(fam$V6=="1"),]$V2))


#### create vector of population labels -- use CASE/CTRL for Cohort subjects
popidx=dx_df$DX



##############################################################
####### generate combined sample DF for plotting
##############################################################

plot_df = data.frame(a[,])
plot_df$DX = popidx

## reorder columns
plot_df = plot_df[, c(c("FID", "IID", "DX"), sprintf("PC%02d",1:10))]
evec_df = plot_df[, c(c("FID", "IID", "DX"), sprintf("PC%02d",1:10))]

## add plotting settings
config_df = data.frame(POP=c("CASE", "CTRL"), Color=c("red", "blue"))
plot_df$COL=config_df$Color[match(plot_df$DX,config_df$POP)]
plot_df$Pch=16
plot_df$Cex=1.0


write.table(evec_df, file=file_evec_df, quote=F, row.names=F, col.names=T, sep="\t")
write.table(plot_df, file=file_plot_df, quote=F, row.names=F, col.names=T, sep="\t")



##############################################################
####### PLOT PCA Functions
##############################################################

plotPCs<-function(df_plot, pcX, pcY, title){

  par(xpd=TRUE)
  plot(df_plot[,c(pcX, pcY)],
       col=df_plot$COL,
       pch=df_plot$Pch,
       cex=df_plot$Cex,
       xlab=pcX,
       ylab=pcY,
       main=paste(title, " - ", pcX, " vs ", pcY, sep = ''))
  
  legend(legend=unique(df_plot$DX),
         col=unique(df_plot$COL),
         pch=16, pt.cex=1, cex=0.6,
         horiz=TRUE, "top", 
         inset=c(0,-0.05), bty="n", xjust=0.5) 
}


makePlot<-function() {
  title <- paste("SmartPCA: ", cohort, sep='')
  
  #### generate plots as pdf file:
  pdf(file_plot_pdf, width=8, height=8, points=14)
  par(xpd=TRUE)
  plotPCs(df_plot=plot_df, pcX="PC01", pcY="PC02", title=title)
  plotPCs(df_plot=plot_df, pcX="PC01", pcY="PC03", title=title)
  plotPCs(df_plot=plot_df, pcX="PC02", pcY="PC03", title=title)
  dev.off()
  
  png(gsub(".pdf", "-PC1_vs_PC2.png", file_plot_pdf))
  plotPCs(df_plot=plot_df, pcX="PC01", pcY="PC02", title=title)
  dev.off()
  
  png(gsub(".pdf", "-PC1_vs_PC3.png", file_plot_pdf))
  plotPCs(df_plot=plot_df, pcX="PC01", pcY="PC03", title=title)
  dev.off()
  
  png(gsub(".pdf", "-PC2_vs_PC3.png", file_plot_pdf))
  plotPCs(df_plot=plot_df, pcX="PC02", pcY="PC03", title=title)
  dev.off()
  
}


##############################################################
####### Call plot function
##############################################################
makePlot()

