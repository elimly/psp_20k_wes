#!/bin/bash

## eigenstrat01_1000G_common_merge-PSP_ADSP.20504.sh

## *NOTE* requires 10G memory (1000G large file size)
## To Run: qsub -l h_vmem=10G ./eigenstrat01_1000G_common_merge-PSP_ADSP.20504.sh


source eigenstrat00_1000G_common_config-PSP_ADSP.20504.sh

mkdir -p $DIR_00_WS $DIR_01_SNPLIST $DIR_02_COMMON $DIR_03_MERGE $DIR_04_MERGE_PRUNE $DIR_05_PCA
mkdir -p "${DIR_COHORT}"/stat "${DIR_1000G}"/stat
# mkdir -p "${DIR_COHORT}"/pruned "${DIR_1000G}"/pruned


####	Part 1:	Compute missingness --> retain markers with missingness rate <5%
## COHORT
echo "Cohort compute missingness"
$PLINKCMD --bfile $COHORT_PREFIX1 --missing --out "${COHORT_PREFIX2}"_missing

$AWK '$2!="SNP" && $5<='$MISS' {print $2}' "${COHORT_PREFIX2}"_missing.lmiss > "${COHORT_PREFIX2}"_keep_lmiss_marker.id
echo 'Number of markers to keep: '`wc -l "${COHORT_PREFIX2}"_keep_lmiss_marker.id`

$AWK '$2!="SNP" && $5>'$MISS' {print $2}' "${COHORT_PREFIX2}"_missing.lmiss > "${COHORT_PREFIX2}"_drop_lmiss_marker.id
echo 'Number of markers to drop: '`wc -l "${COHORT_PREFIX2}"_drop_lmiss_marker.id`

## 1000G phase 3
echo "1000 Genomes compute missingness"
$PLINKCMD --bfile $G1000_PREFIX1 --missing --out "${G1000_PREFIX2}"_missing

$AWK '$2!="SNP" && $5<='$MISS' {print $2}' "${G1000_PREFIX2}"_missing.lmiss > "${G1000_PREFIX2}"_keep_lmiss_marker.id
echo 'Number of markers to keep: '`wc -l "${G1000_PREFIX2}"_keep_lmiss_marker.id`

$AWK '$2!="SNP" && $5>'$MISS' {print $2}' "${G1000_PREFIX2}"_missing.lmiss > "${G1000_PREFIX2}"_drop_lmiss_marker.id
echo 'Number of markers to drop: '`wc -l "${G1000_PREFIX2}"_drop_lmiss_marker.id`


####	Part 2:	Compute frequency --> retain markers with MAF >5%
## Cohort
echo "Cohort compute frequency"
$PLINKCMD --bfile $COHORT_PREFIX1 --freq --out "${COHORT_PREFIX2}"_maf

$AWK '$5<'$MAF' {print $2}' "${COHORT_PREFIX2}"_maf.frq > "${COHORT_PREFIX2}"_drop_maf_marker.id
echo 'Number of markers to drop with MAF<'$MAF': '`wc -l "${COHORT_PREFIX2}"_drop_maf_marker.id`

$AWK '$5>='$MAF' {print $2}' "${COHORT_PREFIX2}"_maf.frq > "${COHORT_PREFIX2}"_keep_maf_marker.id
echo 'Number of markers to keep with MAF>='$MAF': '`wc -l "${COHORT_PREFIX2}"_keep_maf_marker.id`

## 1000 Genomes
echo "1000 Genomes compute frequency"
$PLINKCMD --bfile $G1000_PREFIX1 --freq --out "${G1000_PREFIX2}"_maf

$AWK '$5<'$MAF' {print $2}' "${G1000_PREFIX2}"_maf.frq > "${G1000_PREFIX2}"_drop_maf_marker.id
echo 'Number of markers to drop with MAF<'$MAF': '`wc -l "${G1000_PREFIX2}"_drop_maf_marker.id`

$AWK '$5>='$MAF' {print $2}' "${G1000_PREFIX2}"_maf.frq > "${G1000_PREFIX2}"_keep_maf_marker.id
echo 'Number of markers to keep with MAF>='$MAF': '`wc -l "${G1000_PREFIX2}"_keep_maf_marker.id`



#### Part 3: intersect SNP lists -> identify common SNPs  
echo "Intersect SNP lists"
comm -12 <(sort "${COHORT_PREFIX2}"_keep_lmiss_marker.id) <(sort "${COHORT_PREFIX2}"_keep_maf_marker.id) > "${COHORT_PREFIX2}"_keep_intersect-maf-missing_marker.id
echo `wc -l "${COHORT_PREFIX2}"_keep_intersect-maf-missing_marker.id`

comm -12 <(sort "${G1000_PREFIX2}"_keep_lmiss_marker.id) <(sort "${G1000_PREFIX2}"_keep_maf_marker.id) > "${G1000_PREFIX2}"_keep_intersect-maf-missing_marker.id
echo `wc -l "${G1000_PREFIX2}"_keep_intersect-maf-missing_marker.id`

## copy SNP lists to workspace
cp "${COHORT_PREFIX2}"_keep_intersect-maf-missing_marker.id "${COHORT_SNPLIST}"
cp "${G1000_PREFIX2}"_keep_intersect-maf-missing_marker.id "${G1000_SNPLIST}"

# comm -12 <(sort "${COHORT_PREFIX2}"_keep_intersect-maf-missing_marker.id) <(sort "${G1000_PREFIX2}"_keep_intersect-maf-missing_marker.id) > "${COMMON_SNPLIST}"
comm -12 <(sort "${COHORT_SNPLIST}") <(sort "${G1000_SNPLIST}") > "${COMMON_SNPLIST}"
echo `wc -l "${COMMON_SNPLIST}"`



#### Part 4: extract common SNPs
echo "Extract common SNPs"
$PLINKCMD --bfile $COHORT_PREFIX1 --extract $COMMON_SNPLIST --recode --make-bed --out $COHORT_PREFIX3 
$PLINKCMD --bfile $G1000_PREFIX1 --extract $COMMON_SNPLIST --recode --make-bed --out $G1000_PREFIX3 


#### Part 5: merge extracted common pruned SNPs
echo "Merge extracted common SNPs"

## merge -- round 1
$PLINKCMD --bfile $COHORT_PREFIX3 --bmerge $G1000_PREFIX3.bed $G1000_PREFIX3.bim $G1000_PREFIX3.fam --make-bed --out $MERGE_PREFIX1

## exclude multi allelic SNPs
$PLINKCMD --bfile "${COHORT_PREFIX3}"  --exclude "${MERGE_PREFIX1}"-merge.missnp --make-bed --out "${COHORT_PREFIX3}"_nomiss
$PLINKCMD --bfile "${G1000_PREFIX3}"  --exclude "${MERGE_PREFIX1}"-merge.missnp --make-bed --out "${G1000_PREFIX3}"_nomiss

## merge -- round 2: no miss
$PLINKCMD --bfile "${COHORT_PREFIX3}"_nomiss --bmerge "${G1000_PREFIX3}"_nomiss --make-bed --out $MERGE_PREFIX1


#### Part 6: LD prune merged PLINK set  
echo "LD prune merged data"
$PLINKCMD --bfile $MERGE_PREFIX1 --indep-pairwise 50 5 0.2 --allow-no-sex --out $MERGE_PREFIX2 --write-snplist 


### Part 7: sample & extract 20k SNP markers for PCA 
echo "sample LD pruned SNPs"

# sample 20k SNPs sampled from LD prune.in:
$R PRUNEPREFIX=$MERGE_PREFIX2 INBIM=$MERGE_PREFIX1.bim NSAMPLE=$NSAMPLEMARKER4 < $RSCRIPT_SAMPLE_PRUNED

echo 'Number of autosomal markers after LD pruning: '`wc -l $MERGE_PREFIX2.prune.in.autosomal.id`

## extract 20k SNPs sampled from LD pruned PLINK dataset:
$PLINKCMD --bfile $MERGE_PREFIX1 --extract $MERGE_PREFIX2.prune.in.autosomal.sample.id --recode --make-bed --out $PCA_PREFIX 


echo "eigenstrat01_1000G_merge-PSP_ADSP.20504.sh complete. goodbye."
