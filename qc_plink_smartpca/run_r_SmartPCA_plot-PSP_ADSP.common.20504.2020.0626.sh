#!/bin/bash

COHORT_NAME="psp_adsp" ## in eigenstrat00
PCA_DIR="/mnt/adsp/users/psp_hg38_wes/data/PLINK/eigenstrat_case_ctrl_common" ## in eigenstrat00

OUTPUT_DIR=$PCA_DIR/post_PCA ## in eigenstrat00

PCA_EVEC=$PCA_DIR/PCA/psp_ld_pruned_20k_outliers_removed.a.pca.evec
PCA_FAM=$PCA_DIR/PCA/psp_ld_pruned_20k_outliers_removed.fam


## Rscript
R_SCRIPT_PATH="/mnt/adsp/users/psp_hg38_wes/src/PLINK/03_eigenstrat_GWAS/r_SmartPCA_plot.R"


##--------------------------------------------------------------------------##

mkdir -p $OUTPUT_DIR

#### Run Rscript

echo "Run "$(basename "$R_SCRIPT_PATH")

Rscript "${R_SCRIPT_PATH}" \
	PCA_EVEC="${PCA_EVEC}" \
	PCA_FAM="${PCA_FAM}" \
	OUTPUT_DIR="${OUTPUT_DIR}" \
	COHORT="${COHORT_NAME}" 


