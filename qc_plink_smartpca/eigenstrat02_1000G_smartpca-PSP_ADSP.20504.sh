#!/bin/bash

source eigenstrat00_1000G_config-PSP_ADSP.20504.sh

## MUST ADD smartpca to $PATH or will NOT work!
PATH=$EIGENSTRAT:$PATH


## Copy .fam file & update pheno value -9 -> 1
cp ${PCA_PREFIX}.fam ${PCA_PREFIX}.orig_pheno.fam
awk '{ if ($6 == -9 || $6 == 0 ) $6=1; print $0 }' ${PCA_PREFIX}.orig_pheno.fam > ${PCA_PREFIX}.fam


## Configure settings & convert files for SmartPCA input
echo 'genotypename:    '$PCA_PREFIX'.bed '    > $DIR_05_PCA/$PCA_NAME.smartpca_convert.par
echo 'snpname:         '$PCA_PREFIX'.bim '    >> $DIR_05_PCA/$PCA_NAME.smartpca_convert.par
echo 'indivname:       '$PCA_PREFIX'.fam'     >> $DIR_05_PCA/$PCA_NAME.smartpca_convert.par
echo 'outputformat:    PACKEDPED'              >> $DIR_05_PCA/$PCA_NAME.smartpca_convert.par
echo 'genotypeoutname: '$PCA_PREFIX'.a.bed'     >> $DIR_05_PCA/$PCA_NAME.smartpca_convert.par
echo 'snpoutname:      '$PCA_PREFIX'.a.pedsnp'  >> $DIR_05_PCA/$PCA_NAME.smartpca_convert.par
echo 'indivoutname:    '$PCA_PREFIX'.a.pedind'  >> $DIR_05_PCA/$PCA_NAME.smartpca_convert.par
echo 'familynames:     NO'                     >> $DIR_05_PCA/$PCA_NAME.smartpca_convert.par

$SMARTPCACONVERT -p $DIR_05_PCA/$PCA_NAME.smartpca_convert.par


## Run SmartPCA
echo " ....... Starting SmartPCA"

$SMARTPCA \
		-a ${PCA_PREFIX}.a.pedsnp \
		-b ${PCA_PREFIX}.a.pedind \
		-i ${PCA_PREFIX}.a.bed \
		-e ${PCA_PREFIX}.a.eval \
		-l ${PCA_PREFIX}.a.pcalog \
		-o ${PCA_PREFIX}.a.pca \
		-p ${PCA_PREFIX}.a.plot \
		-m $SMARTPCAMAXITER \
		-s $SMARTPCASIGMATH \
		-t 10 


echo " ....... SmartPCA complete. Goodbye."
