#!/bin/bash


COHORT_NAME="psp_adsp" ## in eigenstrat00
PCA_DIR="/mnt/adsp/users/psp_hg38_wes/data/PLINK/eigenstrat_1000G_common/05b_pca_1000G_common_not_pruned" ## in eigenstrat00
OUTPUT_DIR=$PCA_DIR/post_PCA ## in eigenstrat00

PCA_EVEC=$PCA_DIR/psp_1000G_merge.a.pca.evec
PCA_FAM=$PCA_DIR/psp_1000G_merge.fam

## 1000G files
OUTLIERS=$OUTPUT_DIR/psp_1000G_merge_nonNHW_outliers.txt

POP_SAMPLE="/mnt/adsp/users/psp_hg38_wes/data/PLINK/eigenstrat_1000G_common/pop_input_1000G/relationships_w_pops-1000G.txt"
POP_COLOR="/mnt/adsp/users/psp_hg38_wes/data/PLINK/eigenstrat_1000G_common/pop_input_1000G/colortext-1000G_superpop_sort.txt"

## Rscript
R_SCRIPT_PATH="/mnt/adsp/users/psp_hg38_wes/src/PLINK/03_eigenstrat_GWAS/r_1000G_PCA_plot.R"


##--------------------------------------------------------------------------##

mkdir -p $OUTPUT_DIR

#### Run Rscript

echo "Run "$(basename "$R_SCRIPT_PATH")


Rscript "${R_SCRIPT_PATH}" \
	PCA_EVEC="${PCA_EVEC}" \
	PCA_FAM="${PCA_FAM}" \
	OUTPUT_DIR="${OUTPUT_DIR}" \
	COHORT="${COHORT_NAME}" \
	POP_SAMPLE="${POP_SAMPLE}" \
	POP_COLOR="${POP_COLOR}" \
	OUTLIERS="${OUTLIERS}"

