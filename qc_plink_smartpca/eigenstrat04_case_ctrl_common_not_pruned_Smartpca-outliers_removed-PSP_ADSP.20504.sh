#!/bin/bash

source eigenstrat00_1000G_common_config-PSP_ADSP.20504.sh

# mkdir -p $DIR_06_PCA $DIR_OUT_PCA2 $PCA_DIR2
# mkdir -p "${DIR_06_PCA}"/common_outlier_removed
# mkdir -p "${DIR_06_PCA}"/pruned


# echo "input plink:       "$COHORT_PREFIX1
# echo "extract snps:      "$COHORT_SNPLIST
# echo "remove outliers:   "$OUTLIER 
# echo "pca input plink:   "$PCA_PREFIX2 



DIR_PCA="${DIR_06_PCA}"/PCA_not_pruned
PCA_PREFIX2_B="${DIR_PCA}"/"${COHORT_ABV}""_common_not_pruned_outliers_removed"

mkdir -p $DIR_PCA

ln -s "${COHORT_PREFIX4}".bed "${PCA_PREFIX2_B}".bed
ln -s "${COHORT_PREFIX4}".bim "${PCA_PREFIX2_B}".bim
ln -s "${COHORT_PREFIX4}".fam "${PCA_PREFIX2_B}".fam
ln -s "${COHORT_PREFIX4}".ped "${PCA_PREFIX2_B}".ped
ln -s "${COHORT_PREFIX4}".map "${PCA_PREFIX2_B}".map


# DIR_PCA="${DIR_00_WS}"/05b_pca_1000G_common_not_pruned
# PCA_PREFIX_B="${DIR_PCA}"/"${MERGE_NAME}"

# mkdir -p $DIR_PCA

# ln -s "${MERGE_PREFIX1}".bed "${DIR_PCA}"/"${MERGE_NAME}".bed
# ln -s "${MERGE_PREFIX1}".bim "${DIR_PCA}"/"${MERGE_NAME}".bim
# ln -s "${MERGE_PREFIX1}".fam "${DIR_PCA}"/"${MERGE_NAME}".fam
# ln -s "${MERGE_PREFIX1}".ped "${DIR_PCA}"/"${MERGE_NAME}".ped
# ln -s "${MERGE_PREFIX1}".map "${DIR_PCA}"/"${MERGE_NAME}".map




# #### Part #1: extract common SNPs & remove outlier samples
# echo 'Step 1: extract common SNPs & remove outlier samples'

# $PLINKCMD --bfile $COHORT_PREFIX1 \
# 		--extract "${COHORT_SNPLIST}" \
# 		--remove $OUTLIER \
# 		--recode --make-bed \
# 		--out $COHORT_PREFIX4 



# #### Part #2: extract LD pruned SNPs from clean PLINK dataset
# echo 'Step 2: LD prune SNP set '

# $PLINKCMD --bfile $COHORT_PREFIX4 \
# 		--indep-pairwise 50 5 0.2 --allow-no-sex \
# 		--out $COHORT_PREFIX5 --write-snplist 


# #### Part #3: sample 20k SNPs sampled from LD prune.in:
# echo 'Step 3: sample 20k LD pruned markers for PCA'

# $R PRUNEPREFIX=$COHORT_PREFIX5 INBIM=$COHORT_PREFIX4.bim NSAMPLE=$NSAMPLEMARKER4 < $RSCRIPT_SAMPLE_PRUNED
# echo 'Number of autosomal markers after LD pruning: '`wc -l $COHORT_PREFIX5.prune.in.autosomal.id`


# #### Part #4: extract 20k SNPs sampled from LD pruned PLINK dataset
# echo 'Step 4: extract 20k SNPs sampled from LD pruned PLINK dataset'

# $PLINKCMD --bfile $COHORT_PREFIX4 \
# 		--extract $COHORT_PREFIX5.prune.in.autosomal.sample.id \
# 		--recode --make-bed --out $PCA_PREFIX2 --write-snplist


#### Part #5: Configure settings & convert files for SmartPCA input
echo 'Step 5: SmartPCA convertf configure settings & convert files for SmartPCA'

echo 'genotypename:    '$PCA_PREFIX2_B'.bed '    > $PCA_PREFIX2_B.smartpca_convert.par
echo 'snpname:         '$PCA_PREFIX2_B'.bim '    >> $PCA_PREFIX2_B.smartpca_convert.par
echo 'indivname:       '$PCA_PREFIX2_B'.fam'     >> $PCA_PREFIX2_B.smartpca_convert.par
echo 'outputformat:    PACKEDPED'              >> $PCA_PREFIX2_B.smartpca_convert.par
echo 'genotypeoutname: '$PCA_PREFIX2_B'.a.bed'     >> $PCA_PREFIX2_B.smartpca_convert.par
echo 'snpoutname:      '$PCA_PREFIX2_B'.a.pedsnp'  >> $PCA_PREFIX2_B.smartpca_convert.par
echo 'indivoutname:    '$PCA_PREFIX2_B'.a.pedind'  >> $PCA_PREFIX2_B.smartpca_convert.par
echo 'familynames:     NO'                     >> $PCA_PREFIX2_B.smartpca_convert.par

$SMARTPCACONVERT -p $PCA_PREFIX2_B.smartpca_convert.par


#### Part #6: Run SmartPCA
echo 'Step 6: Run SmartPCA'

$SMARTPCA \
		-a ${PCA_PREFIX2_B}.a.pedsnp \
		-b ${PCA_PREFIX2_B}.a.pedind \
		-i ${PCA_PREFIX2_B}.a.bed \
		-e ${PCA_PREFIX2_B}.a.eval \
		-l ${PCA_PREFIX2_B}.a.pcalog \
		-o ${PCA_PREFIX2_B}.a.pca \
		-p ${PCA_PREFIX2_B}.a.pca.par \
		-s $SMARTPCASIGMATH \
		-t 10


echo " ....... SmartPCA complete. Goodbye."
