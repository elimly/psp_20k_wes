#!/bin/bash

source eigenstrat00_1000G_common_config-PSP_ADSP.20504.sh

## MUST ADD smartpca to $PATH or will NOT work!
# PATH=$EIGENSTRAT:$PATH

DIR_PCA="${DIR_00_WS}"/05b_pca_1000G_common_not_pruned
PCA_PREFIX_B="${DIR_PCA}"/"${MERGE_NAME}"

mkdir -p $DIR_PCA

ln -s "${MERGE_PREFIX1}".bed "${DIR_PCA}"/"${MERGE_NAME}".bed
ln -s "${MERGE_PREFIX1}".bim "${DIR_PCA}"/"${MERGE_NAME}".bim
ln -s "${MERGE_PREFIX1}".fam "${DIR_PCA}"/"${MERGE_NAME}".fam
ln -s "${MERGE_PREFIX1}".ped "${DIR_PCA}"/"${MERGE_NAME}".ped
ln -s "${MERGE_PREFIX1}".map "${DIR_PCA}"/"${MERGE_NAME}".map


## Copy .fam file & update pheno value -9 -> 1
cp ${PCA_PREFIX_B}.fam ${PCA_PREFIX_B}.orig_pheno.fam
awk '{ if ($6 == -9 || $6 == 0 ) $6=1; print $0 }' ${PCA_PREFIX_B}.orig_pheno.fam > ${PCA_PREFIX_B}.fam


## Configure settings & convert files for SmartPCA input
echo 'genotypename:    '$PCA_PREFIX_B'.bed '    > $DIR_PCA/$MERGE_NAME.smartpca_convert.par
echo 'snpname:         '$PCA_PREFIX_B'.bim '    >> $DIR_PCA/$MERGE_NAME.smartpca_convert.par
echo 'indivname:       '$PCA_PREFIX_B'.fam'     >> $DIR_PCA/$MERGE_NAME.smartpca_convert.par
echo 'outputformat:    PACKEDPED'              >> $DIR_PCA/$MERGE_NAME.smartpca_convert.par
echo 'genotypeoutname: '$PCA_PREFIX_B'.a.bed'     >> $DIR_PCA/$MERGE_NAME.smartpca_convert.par
echo 'snpoutname:      '$PCA_PREFIX_B'.a.pedsnp'  >> $DIR_PCA/$MERGE_NAME.smartpca_convert.par
echo 'indivoutname:    '$PCA_PREFIX_B'.a.pedind'  >> $DIR_PCA/$MERGE_NAME.smartpca_convert.par
echo 'familynames:     NO'                     >> $DIR_PCA/$MERGE_NAME.smartpca_convert.par

$SMARTPCACONVERT -p $DIR_PCA/$MERGE_NAME.smartpca_convert.par


## Run SmartPCA
echo " ....... Starting SmartPCA"

$SMARTPCA \
		-a ${PCA_PREFIX_B}.a.pedsnp \
		-b ${PCA_PREFIX_B}.a.pedind \
		-i ${PCA_PREFIX_B}.a.bed \
		-e ${PCA_PREFIX_B}.a.eval \
		-l ${PCA_PREFIX_B}.a.pcalog \
		-o ${PCA_PREFIX_B}.a.pca \
		-p ${PCA_PREFIX_B}.a.plot \
		-m $SMARTPCAMAXITER \
		-s $SMARTPCASIGMATH \
		-t 10 


echo " ....... SmartPCA complete. Goodbye."
